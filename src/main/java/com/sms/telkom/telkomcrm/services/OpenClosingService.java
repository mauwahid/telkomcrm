package com.sms.telkom.telkomcrm.services;

import com.sms.telkom.telkomcrm.dtos.ReceiveSmsResponse;
import com.sms.telkom.telkomcrm.entities.MOFailed;
import com.sms.telkom.telkomcrm.entities.MOSuccess;
import com.sms.telkom.telkomcrm.entities.SmsResolvedActive;
import com.sms.telkom.telkomcrm.rabbitmq.TelkomSender;
import com.sms.telkom.telkomcrm.repositories.MOFailedRepository;
import com.sms.telkom.telkomcrm.repositories.MOSuccesRepository;
import com.sms.telkom.telkomcrm.repositories.SmsResolvedActiveRepository;
import com.sms.telkom.telkomcrm.utils.SmsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class OpenClosingService {

    @Autowired
    private SmsResolvedActiveRepository smsResolvedActiveRepository;

    @Autowired
    private SendToTelkomService sendToTelkomService;

    @Autowired
    private TelkomSender telkomSender;

    @Autowired
    private MOSuccesRepository moSuccesRepository;

    @Autowired
    private MOFailedRepository moFailedRepository;


    public ReceiveSmsResponse processClosing(String encodeMsisdn, String shortCode,
                                             String content, String ticketNum, String answer, String telco, String trxId){

        ReceiveSmsResponse receiveSmsResponse = new ReceiveSmsResponse();
        SmsResolvedActive smsResolvedActive = smsResolvedActiveRepository.findByTicketNum(ticketNum);

        if(smsResolvedActive != null){
            smsResolvedActive.setReplyContent(content);
            smsResolvedActive.setShortCode(shortCode);
            smsResolvedActive.setAnswer(answer);
            byte answered = 1;

            if(answer.equalsIgnoreCase("Y")){
                receiveSmsResponse.setResponseString(SmsResponse.SMS_YES_OPEN_CLOSE);
            }else if(answer.equalsIgnoreCase("N")){
                receiveSmsResponse.setResponseString(SmsResponse.SMS_NO_OPEN_CLOSE);
            }else{
                if(shortCode == "1147")
                    receiveSmsResponse.setResponseString(SmsResponse.SMS_SALAH);
                else
                    receiveSmsResponse.setResponseString(SmsResponse.SMS_SALAH_2000);
                answered = 0;
            }

            smsResolvedActive.setIsAnswered(answered);
            smsResolvedActiveRepository.save(smsResolvedActive);
            saveMO(shortCode, encodeMsisdn, smsResolvedActive.getMsisdn(), ticketNum, answer, telco, trxId);

            if(smsResolvedActive.getIsAnswered() == 1){
                log.debug("start send to telkom");
                Map<String, String> messages = new HashMap<>();
                messages.put("msisdn",smsResolvedActive.getMsisdn());
                messages.put("answer", smsResolvedActive.getAnswer());
                messages.put("ticketNum", smsResolvedActive.getTicketNum());
                messages.put("id", smsResolvedActive.getId()+"");

                telkomSender.sendToTelkom(messages);
            //    sendToTelkomService.sendToTelkom(smsResolvedActive);
            }

        }else{
            saveMoFailed(encodeMsisdn, content, shortCode, SmsResponse.SMS_NO_TICKET_ID, trxId);
            receiveSmsResponse.setResponseString(SmsResponse.SMS_NO_TICKET_ID);
        }

        return receiveSmsResponse;


    }


    private void saveMO(String shortCode, String encodeMsidn, String msisdn, String ticketNum,String answer, String telco, String trxId ){

        MOSuccess moSuccess = new MOSuccess();
        moSuccess.setShortCode(shortCode);
        moSuccess.setEncryptedMsisdn(encodeMsidn);
        moSuccess.setMsisdn(msisdn);
        moSuccess.setTicketNumber(ticketNum);
        moSuccess.setAnswer(answer);
        moSuccess.setTelco(telco);
        moSuccess.setTrxId(trxId);

        moSuccesRepository.save(moSuccess);

    }

    private void saveMoFailed(String encodeMsisdn, String content, String shortCode, String replyToCustomer, String trxId){

        MOFailed moFailed = new MOFailed();
        moFailed.setContent(content);
        moFailed.setEncryptedMsisdn(encodeMsisdn);
        moFailed.setShortCode(shortCode);
        moFailed.setReplyToCustomer(replyToCustomer);
        moFailed.setTrxId(trxId);

        moFailedRepository.save(moFailed);
    }

}
