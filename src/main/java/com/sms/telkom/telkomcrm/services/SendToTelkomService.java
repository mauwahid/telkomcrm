package com.sms.telkom.telkomcrm.services;

import com.sms.telkom.telkomcrm.entities.SmsResolvedActive;
import com.sms.telkom.telkomcrm.repositories.SmsResolvedActiveRepository;
import lombok.extern.slf4j.Slf4j;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class SendToTelkomService {

    private String url = "http://servicebus.telkom.co.id:9001/CEMProject/TelkomComposite/Assurance/ProxyService/smsFaultHandling";


    @Autowired
    private SmsResolvedActiveRepository smsResolvedActiveRepository;

    private String sendRequest(String uri, Map<String, String> params)
            throws IOException {

        //  OkHttpClient client = new OkHttpClient();
        log.debug("URI "+uri.toString());
        // uri = uri+"lion/search/best_price";
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        OkHttpClient client = builder.build();

        HttpUrl.Builder urlBuilder = HttpUrl.parse(uri).newBuilder();

        params.forEach((k,v)->
                {
                    urlBuilder.addQueryParameter(k,v);

                }
        );

        String url = urlBuilder.build().toString();

        log.debug("url : "+url);

        Request request = new Request.Builder().
                url(url).build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    public void sendToTelkom(SmsResolvedActive smsResolvedActive){

        Map<String, String> params = new HashMap<>();
        params.put("msisdn", smsResolvedActive.getMsisdn());
        params.put("trxId", smsResolvedActive.getVendorTrxId());
        params.put("sms", "TLKM "+ smsResolvedActive.getAnswer());
        params.put("tiketId", smsResolvedActive.getTicketNum());


        String response = "";

        try{
            response = sendRequest(url, params);
        }catch (Exception ex){
            response = ex.toString();
        }

        int status = 0;

        if(response.toLowerCase().contains("<status>1</status>")){
            status = 1;
        }else{
            status = 2;
        }

        long milis = System.currentTimeMillis();
        smsResolvedActive.setSentToTelkomDate(new Date(milis));
        smsResolvedActive.setSentToTelkomStatus(status);
        if(status == 2)
            smsResolvedActive.setSentToTelkomDesc(response);

        smsResolvedActiveRepository.save(smsResolvedActive);
        log.debug("end send to telkom");
    }





}
