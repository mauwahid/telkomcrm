package com.sms.telkom.telkomcrm.services;

import org.springframework.data.domain.Page;

import java.util.List;

public interface CrudService<T> {
    Iterable<T> listAll();

    T getById(Long id);

    T saveOrUpdate(T domainObject);

    void delete(T domainObject);

    // Page<T> listPage(Pageable pageable);

    Page<T> getPage(int pageNumber);
}