package com.sms.telkom.telkomcrm.services;

import com.sms.telkom.telkomcrm.dtos.ReceiveSmsResponse;
import com.sms.telkom.telkomcrm.dtos.TicketNumber;
import com.sms.telkom.telkomcrm.entities.MOFailed;
import com.sms.telkom.telkomcrm.repositories.MOFailedRepository;
import com.sms.telkom.telkomcrm.repositories.MasterTicketRepository;
import com.sms.telkom.telkomcrm.utils.SmsResponse;
import com.sms.telkom.telkomcrm.utils.TypeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class Receive2000Service {

    @Autowired
    private MasterTicketRepository masterTicketRepository;

    @Autowired
    private OpenClosingService openClosingService;

    @Autowired
    private MOFailedRepository moFailedRepository;


    public ReceiveSmsResponse smsprocessResponse(String encodedMsisdn, String shortCode, String content, String telco, String trxId){

        TicketNumber ticketNumber = extractTicket(content);

        ReceiveSmsResponse receiveSmsResponse = null;

        switch (ticketNumber.getType()){
            case TypeUtils.RECEIVE_SMS_RESOLVED:
                receiveSmsResponse = processOpenClosingTicket(encodedMsisdn, shortCode, content, ticketNumber.getTicketNumber(), ticketNumber.getAnswer(), telco, trxId );

                break;
            default:
                saveMoFailed(encodedMsisdn, content, shortCode, SmsResponse.SMS_SALAH_2000, trxId);
                receiveSmsResponse = new ReceiveSmsResponse();
                receiveSmsResponse.setResponseString(SmsResponse.SMS_SALAH_2000);
        }

        return  receiveSmsResponse;
    }



    private TicketNumber extractTicket(String content){


        //TLKM NoTicket Y/N

        log.debug("content "+content);
        String answer = "";

        String ticketId = "";

        int idxSpace = 0;
        try{
            idxSpace = content.indexOf(" ",5);
            ticketId = content.substring(5, idxSpace);
        }catch (Exception ex){
            ticketId = "";
        }

        try {
            answer = content.substring(idxSpace+1, idxSpace+2);
        }catch (Exception ex){
            answer = "";
        }

        log.debug("ticket "+ticketId+", answer "+answer);

        int type = TypeUtils.RECEIVE_INVALID_TICKET_FORMAT;

        try{
            if(ticketId.toUpperCase().startsWith("IN")){
                type = TypeUtils.RECEIVE_SMS_RESOLVED;
            }else if(ticketId.toUpperCase().startsWith("HD")){
                type = TypeUtils.RECEIVE_HD_SMS;
            }else if(ticketId.toUpperCase().startsWith("P")){
                type = TypeUtils.RECEIVE_PROFILING;
            }else if(ticketId.toUpperCase().startsWith("GL")){
                type = TypeUtils.RECEIVE_GL;
            }
        }catch (Exception ex){

        }


        /*else if(Character.isDigit(ticketId.charAt(0)) & answer.toUpperCase().equalsIgnoreCase("Y")
                | ticketId.toUpperCase().startsWith("S")){
            type = TypeUtils.RECEIVE_SURVEY_1;
        }else if(Character.isDigit(ticketId.charAt(0)) & answer.toUpperCase().equalsIgnoreCase("N")
                | ticketId.toUpperCase().startsWith("TDK") | ticketId.toUpperCase().startsWith("YA")){
            type = TypeUtils.RECEIVE_SURVEY_2;
        } */

        TicketNumber ticketNumber = new TicketNumber();
        ticketNumber.setAnswer(answer.toUpperCase());
        ticketNumber.setTicketNumber(ticketId);
        ticketNumber.setType(type);

        return ticketNumber;

    }


    private ReceiveSmsResponse processOpenClosingTicket(String encodeMsisdn, String shortCode,
                                                        String content, String ticketId, String answer,
                                                        String telco, String trxId){
        ReceiveSmsResponse receiveSmsResponse = openClosingService.processClosing(encodeMsisdn, shortCode, content, ticketId, answer, telco, trxId);
        return  receiveSmsResponse;
    }

    private void saveMoFailed(String encodeMsisdn, String content, String shortCode, String replyToCustomer, String trxId){

        MOFailed moFailed = new MOFailed();
        moFailed.setContent(content);
        moFailed.setEncryptedMsisdn(encodeMsisdn);
        moFailed.setShortCode(shortCode);
        moFailed.setReplyToCustomer(replyToCustomer);
        moFailed.setTrxId(trxId);

        moFailedRepository.save(moFailed);
    }





}
