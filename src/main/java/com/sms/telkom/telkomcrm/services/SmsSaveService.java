package com.sms.telkom.telkomcrm.services;

import com.sms.telkom.telkomcrm.dtos.SendSmsReq;
import com.sms.telkom.telkomcrm.entities.*;
import com.sms.telkom.telkomcrm.repositories.*;
import com.sms.telkom.telkomcrm.utils.TypeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SmsSaveService {


    @Autowired
    private SmsResolvedActiveRepository smsResolvedActiveRepository;

    @Autowired
    private SmsResolvedArchiveRepository smsResolvedArchiveRepository;

    @Autowired
    private SmsOthersRepository smsOthersRepository;

    @Autowired
    private SmsLabAssignRepository smsSendToBackendRepository;

    @Autowired
    private SmsOpenRepository smsOpenRepository;

    @Autowired
    private SmsGamasOpenRepository smsGamasOpenRepository;


    @Autowired
    private SmsGamasCloseRepository smsGamasCloseRepository;



    public void saveSms(SendSmsReq sendSmsReq, String contentToCustomer, String senderToCustomer, int smsCount,
                         String operator, String gateway,
                         int status, String errorDesc, Long idUser, String trxId){


        switch (sendSmsReq.getType()){

            case TypeUtils.SMS_RESOLVED:
                saveSmsResolved(sendSmsReq, contentToCustomer, senderToCustomer, smsCount, operator,
                        gateway, status, errorDesc, idUser, trxId);
                break;

            case TypeUtils.SMS_LAB_ASSIGN:
                saveSmsLabAssign(sendSmsReq, contentToCustomer, senderToCustomer, smsCount, operator,
                        gateway, status, errorDesc, idUser, trxId);
                break;
            case TypeUtils.SMS_GAMAS_OPEN:
                saveSmsTicketGamasOpen(sendSmsReq, contentToCustomer, senderToCustomer, smsCount, operator,
                        gateway, status, errorDesc, idUser, trxId);
                break;
            case TypeUtils.SMS_GAMAS_CLOSE:
                saveSmsTicketGamasClose(sendSmsReq, contentToCustomer, senderToCustomer, smsCount, operator,
                        gateway, status, errorDesc, idUser, trxId);
                break;

            case TypeUtils.SMS_OPEN:
                saveSmsOpen(sendSmsReq, contentToCustomer, senderToCustomer, smsCount, operator,
                        gateway, status, errorDesc, idUser, trxId);
                break;
            case TypeUtils.OTHERS :
                saveSmsOther(sendSmsReq, contentToCustomer, senderToCustomer, smsCount, operator,
                        gateway, status, errorDesc, idUser, trxId);
                break;
            default:
                saveSmsOther(sendSmsReq, contentToCustomer, senderToCustomer, smsCount, operator,
                        gateway, status, errorDesc, idUser, trxId);
                break;

        }



    }

    public void saveSmsResolved(SendSmsReq sendSmsReq, String contentToCustomer, String senderToCustomer, int smsCount,
                                String operator, String gateway,
                                int status, String errorDesc, Long idUser, String trxId){


        SmsResolvedActive smsResolvedActive = new SmsResolvedActive();
        smsResolvedActive.setContent(sendSmsReq.getContent());
        smsResolvedActive.setMsisdn(sendSmsReq.getMsisdn());
        smsResolvedActive.setSenderId(sendSmsReq.getSenderId());
        smsResolvedActive.setTicketNum(sendSmsReq.getTicketNum());
        smsResolvedActive.setGateway(gateway);
        smsResolvedActive.setSmsErrorResponse(errorDesc);
        smsResolvedActive.setSentStatus(status);
        smsResolvedActive.setSmsErrorResponse(errorDesc);
        smsResolvedActive.setIdUser(idUser);
        smsResolvedActive.setContentToCustomer(contentToCustomer);
        smsResolvedActive.setSenderIdToCust(senderToCustomer);
        smsResolvedActive.setSmsCount(smsCount);
        smsResolvedActive.setSmsErrorResponse(errorDesc);
        smsResolvedActive.setOperator(operator);
        smsResolvedActive.setVendorTrxId(trxId);

        smsResolvedActiveRepository.save(smsResolvedActive);
    }


   public void saveSmsLabAssign(SendSmsReq sendSmsReq, String contentToCustomer, String senderToCustomer, int smsCount,
                                String operator, String gateway,
                                int status, String errorDesc, Long idUser, String trxId){


        SmsLabAssign sms = new SmsLabAssign();
        sms.setContent(sendSmsReq.getContent());
        sms.setMsisdn(sendSmsReq.getMsisdn());
        sms.setSenderId(sendSmsReq.getSenderId());
        sms.setGateway(gateway);
        sms.setSmsErrorResponse(errorDesc);
        sms.setSentStatus(status);
        sms.setSmsErrorResponse(errorDesc);
        sms.setIdUser(idUser);
        sms.setContentToCustomer(contentToCustomer);
        sms.setSenderIdToCust(senderToCustomer);
        sms.setSmsCount(smsCount);
        sms.setSmsErrorResponse(errorDesc);
        sms.setOperator(operator);
        sms.setVendorTrxId(trxId);

        smsSendToBackendRepository.save(sms);
    }

    public void saveSmsOpen(SendSmsReq sendSmsReq, String contentToCustomer, String senderToCustomer, int smsCount,
                            String operator, String gateway,
                            int status, String errorDesc, Long idUser, String trxId){


        SmsOpen sms = new SmsOpen();
        sms.setContent(sendSmsReq.getContent());
        sms.setMsisdn(sendSmsReq.getMsisdn());
        sms.setSenderId(sendSmsReq.getSenderId());
        sms.setGateway(gateway);
        sms.setSmsErrorResponse(errorDesc);
        sms.setSentStatus(status);
        sms.setSmsErrorResponse(errorDesc);
        sms.setIdUser(idUser);
        sms.setContentToCustomer(contentToCustomer);
        sms.setSenderIdToCust(senderToCustomer);
        sms.setSmsCount(smsCount);
        sms.setSmsErrorResponse(errorDesc);
        sms.setOperator(operator);
        sms.setVendorTrxId(trxId);

        smsOpenRepository.save(sms);
    }

    public void saveSmsTicketGamasOpen(SendSmsReq sendSmsReq, String contentToCustomer, String senderToCustomer, int smsCount,
                                       String operator, String gateway,
                                       int status, String errorDesc, Long idUser, String trxId){


        SmsGamasOpen sms = new SmsGamasOpen();
        sms.setContent(sendSmsReq.getContent());
        sms.setMsisdn(sendSmsReq.getMsisdn());
        sms.setSenderId(sendSmsReq.getSenderId());
        sms.setGateway(gateway);
        sms.setSmsErrorResponse(errorDesc);
        sms.setSentStatus(status);
        sms.setSmsErrorResponse(errorDesc);
        sms.setIdUser(idUser);
        sms.setContentToCustomer(contentToCustomer);
        sms.setSenderIdToCust(senderToCustomer);
        sms.setSmsCount(smsCount);
        sms.setSmsErrorResponse(errorDesc);
        sms.setOperator(operator);
        sms.setVendorTrxId(trxId);

        smsGamasOpenRepository.save(sms);
    }

    public void saveSmsTicketGamasClose(SendSmsReq sendSmsReq, String contentToCustomer, String senderToCustomer, int smsCount,
                                       String operator, String gateway,
                                       int status, String errorDesc, Long idUser, String trxId){


        SmsGamasClose sms = new SmsGamasClose();
        sms.setContent(sendSmsReq.getContent());
        sms.setMsisdn(sendSmsReq.getMsisdn());
        sms.setSenderId(sendSmsReq.getSenderId());
        sms.setGateway(gateway);
        sms.setSmsErrorResponse(errorDesc);
        sms.setSentStatus(status);
        sms.setSmsErrorResponse(errorDesc);
        sms.setIdUser(idUser);
        sms.setContentToCustomer(contentToCustomer);
        sms.setSenderIdToCust(senderToCustomer);
        sms.setSmsCount(smsCount);
        sms.setSmsErrorResponse(errorDesc);
        sms.setOperator(operator);
        sms.setVendorTrxId(trxId);

        smsGamasCloseRepository.save(sms);
    }


    public void saveSmsOther(SendSmsReq sendSmsReq, String contentToCustomer, String senderToCustomer, int smsCount,
                                String operator, String gateway,
                                int status, String errorDesc, Long idUser, String trxId){


        SmsOthers sms = new SmsOthers();
        sms.setContent(sendSmsReq.getContent());
        sms.setMsisdn(sendSmsReq.getMsisdn());
        sms.setSenderId(sendSmsReq.getSenderId());
        sms.setGateway(gateway);
        sms.setSmsErrorResponse(errorDesc);
        sms.setSentStatus(status);
        sms.setSmsErrorResponse(errorDesc);
        sms.setIdUser(idUser);
        sms.setContentToCustomer(contentToCustomer);
        sms.setSenderIdToCust(senderToCustomer);
        sms.setSmsCount(smsCount);
        sms.setSmsErrorResponse(errorDesc);
        sms.setOperator(operator);
        sms.setVendorTrxId(trxId);

        smsOthersRepository.save(sms);
    }

}
