package com.sms.telkom.telkomcrm.services;

import com.sms.telkom.telkomcrm.dtos.SendSmsReq;
import com.sms.telkom.telkomcrm.dtos.SendSmsRes;
import com.sms.telkom.telkomcrm.dtos.SmsApiResponse;
import com.sms.telkom.telkomcrm.entities.MasterTicket;
import com.sms.telkom.telkomcrm.entities.Operator;
import com.sms.telkom.telkomcrm.entities.SmsResolvedActive;
import com.sms.telkom.telkomcrm.repositories.MasterTicketRepository;
import com.sms.telkom.telkomcrm.repositories.OperatorRepository;
import com.sms.telkom.telkomcrm.repositories.SmsResolvedActiveRepository;
import com.sms.telkom.telkomcrm.smsapi.NadyneApiService;
import com.sms.telkom.telkomcrm.smsapi.SmsApiInterface;
import com.sms.telkom.telkomcrm.utils.Common;
import com.sms.telkom.telkomcrm.utils.OperatorUtils;
import com.sms.telkom.telkomcrm.utils.TypeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SendSmsRoutingService {

    @Autowired
    private SmsResolvedActiveRepository smsResolvedActiveRepository;

    @Autowired
    private MasterTicketRepository masterTicketRepository;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private OperatorRepository operatorRepository;

    @Autowired
    private SmsSaveService smsSaveService;


    public SendSmsRes composeSms(SendSmsReq sendSmsReq, Long idUser){

        String operatorName = getOperatorName(sendSmsReq.getMsisdn());
        String senderIdToCust = getSenderId(sendSmsReq.getSenderId(), sendSmsReq.getType(), operatorName);

        String content = getContent(sendSmsReq.getContent(), sendSmsReq.getTicketNum(), sendSmsReq.getType(), operatorName);
        int countSms = countSms(content);

        SendSmsRes sendSmsRes = new SendSmsRes();

        if(sendSmsReq.getType() == TypeUtils.SMS_RESOLVED){
            SmsResolvedActive smsResolvedActive = smsResolvedActiveRepository.findByTicketNum(sendSmsReq.getTicketNum());


            if(smsResolvedActive !=null){
                sendSmsRes.setStatus(Common.STATUS_FAILED);
                sendSmsRes.setStatusDesc(Common.STATUS_FAILED_DESC_TICKET_AVAILABLE);

                return sendSmsRes;
            }

        }


        SmsApiResponse smsApiResponse = processSms(sendSmsReq.getMsisdn(), senderIdToCust, content, operatorName);

        saveSms(sendSmsReq, content, senderIdToCust, countSms, operatorName, smsApiResponse.getGatewayName(),
                smsApiResponse.getStatus(), smsApiResponse.getStatusDesc(),idUser, smsApiResponse.getTrxId());

    //    saveTicket(sendSmsReq.getTicketNum(), sendSmsReq.getMsisdn());


        sendSmsRes.setStatus(smsApiResponse.getStatus());

        if(smsApiResponse.getStatus() == Common.STATUS_SUCCESS){
            sendSmsRes.setStatusDesc(Common.STATUS_SUCCESS_DESC);

        }
        else
            sendSmsRes.setStatusDesc(smsApiResponse.getStatusDesc());

        sendSmsRes.setOperatorName(operatorName);
        sendSmsRes.setSmsCount(countSms);

        return sendSmsRes;
    }


    private SmsApiResponse processSms(String msisdn, String senderId, String content, String operator){

        SmsApiInterface smsInterface = context.getBean(NadyneApiService.class);

        SmsApiResponse smsApiResponse = smsInterface.sendSms(content, senderId, msisdn);

        return smsApiResponse;
    }


    private String getOperatorName(String msisdn){

        String operatorName = "OTHER";

        if(msisdn.startsWith("0")){
            msisdn = "62" + msisdn.substring(1);
        }

        if(msisdn.startsWith("+")){
            msisdn = msisdn.substring(1);
        }

      //  log.debug("MSISDN "+msisdn);

        String prefix = msisdn.substring(0, 5);

      //  log.debug("prefix msisdn "+prefix);

        Operator operator = operatorRepository.findByPrefix(prefix);

        if(operator != null){
            operatorName = operator.getOperatorName();
        }

        return operatorName;

      /*  boolean tsel = false;
        String recipient = "";

        if (msisdn.startsWith("62")) {
            recipient = "0"+msisdn.substring(1);
        }else{
            recipient = msisdn;
        }

        if(recipient.startsWith("0811") | recipient.startsWith("0812") | recipient.startsWith("0813")){
            tsel = true;
        }


        if(recipient.startsWith("0821") | recipient.startsWith("0822") | recipient.startsWith("0823")){
            tsel = true;
        }


        if(recipient.startsWith("0851") | recipient.startsWith("0852") | recipient.startsWith("0853")){
            tsel = true;
        }

        if(tsel)
            return "TSEL";
        else
            return "NONTSEL";
            */


    }


    private String getSenderId(String defaultSender, int type, String operator){

        String senderId = defaultSender;
        if(type == TypeUtils.SMS_RESOLVED){

            if(operator.equalsIgnoreCase(OperatorUtils.TELKOMSEL))
               senderId = "1147";

        }
        log.debug("Sender id "+senderId);
        return senderId;
    }

    private String getContent(String defaultContent, String ticketNum, int type, String operator){

        String content = defaultContent;
        if(type == TypeUtils.SMS_RESOLVED){

            if(operator.equalsIgnoreCase(OperatorUtils.TELKOMSEL)){
                content = content + " Mohon konfirm Y=Sdh baik,N=Blm baik, Balas Y/N "+ticketNum+" (GRATIS)";
            }else{
                content = content + " Mohon konfirm Y=Sdh baik,N=Blm baik, ketik TLKM "+ticketNum+" Y/N kirim ke 2000 (GRATIS)";
            }

        }

        return content;
    }

    private int countSms(String content){
        double perSms = 160;
        double lengthSms = content.length();

        int countSms = (int) Math.ceil(lengthSms / perSms);

        return countSms;

    }

    private void saveSms(SendSmsReq sendSmsReq, String contentToCustomer, String senderToCustomer, int smsCount,
                         String operator, String gateway,
                         int status, String errorDesc, Long idUser,String trxId){

        smsSaveService.saveSms(sendSmsReq, contentToCustomer, senderToCustomer, smsCount, operator,
                gateway, status, errorDesc, idUser, trxId);
    }


    private void saveTicket(String ticketNum, String msisdn){

        MasterTicket masterTicket = masterTicketRepository.findByTicketNum(ticketNum);

        if(masterTicket == null){
            masterTicket = new MasterTicket();
        }

        masterTicket.setMsisdn(msisdn);
        masterTicket.setTicketNum(ticketNum);

        masterTicketRepository.save(masterTicket);

    }










}
