package com.sms.telkom.telkomcrm.entities;

import lombok.Data;

import javax.persistence.Entity;

@Entity
@Data
public class MOFailed extends AbstractEntity {

    private String encryptedMsisdn;

//    private String msisdn;

    private String content;

    private String telco;

//    private String replyFromCustomer;

    private String replyToCustomer;

    private String shortCode;

    private String trxId;
}
