package com.sms.telkom.telkomcrm.entities;

import lombok.Data;

import javax.persistence.Entity;

@Entity
@Data
public class MasterTicket extends AbstractEntity {

    private String msisdn;

    private String encodedMsisdn;

    private String ticketNum;

    private boolean isExist;
}
