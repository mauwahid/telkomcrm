package com.sms.telkom.telkomcrm.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@Data
public class SmsResolvedArchive extends Sms {

    @Column
    private String ticketNum;


    //answered
    private Date answeredDate;

    private byte isAnswered = 0;

    private String answer;

    private String replyContent;

    private String shortCode;

    private int isSentToTelkom;

    private Date sentToTelkomDate;

    private int sentToTelkomStatus;

    private String sentToTelkomDesc;
}
