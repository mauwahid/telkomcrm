package com.sms.telkom.telkomcrm.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Data
public class MOSuccess extends AbstractEntity {

    private String encryptedMsisdn;

    private String msisdn;

    private String ticketNumber;

    private String content;

    @Column(length = 2)
    private String answer;

    private String telco;

  //  private String replyFromCustomer;

    private String replyToCustomer;

    private String shortCode;

    private String trxId;
}
