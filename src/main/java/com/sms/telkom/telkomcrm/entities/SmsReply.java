package com.sms.telkom.telkomcrm.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;
import java.sql.Date;

@Data
@MappedSuperclass
public class SmsReply extends AbstractEntity {

    private String encodedMsisdn;

    private String content;

    private String ticketNum;

    private String shortCode;

    private int sentToTelkomStatus;

    private String errorToTelkom;

    private Date sentToTelkom;


}
