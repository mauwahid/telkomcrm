package com.sms.telkom.telkomcrm.entities;

import lombok.Data;

import javax.persistence.Entity;

@Entity
@Data
public class MOTicketClosing extends AbstractEntity {

    private String encryptedMsisdn;

    private String msisdn;

    private String ticketNumber;

    private String content;

    private String answer;

    private String replyFromCustomer;

    private String replyToCustomer;

    private String shortCode;
}
