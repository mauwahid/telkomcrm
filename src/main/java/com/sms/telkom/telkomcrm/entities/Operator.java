package com.sms.telkom.telkomcrm.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.UniqueConstraint;

@Entity
@Data
public class Operator  extends AbstractEntity{

    @Column(unique = true)
    private String prefix;

    private String operatorName;

}
