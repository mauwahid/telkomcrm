package com.sms.telkom.telkomcrm.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Transient;

@Data
@Entity
public class User extends AbstractEntity {


    private String username;

    private String password;

    private String encryptedPassword;

    @Transient
    private String retypePassword;

    private int role;

}
