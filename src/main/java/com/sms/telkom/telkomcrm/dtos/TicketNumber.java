package com.sms.telkom.telkomcrm.dtos;

import lombok.Data;

@Data
public class TicketNumber {

    private String ticketNumber;
    private String answer;
    private int type;

}
