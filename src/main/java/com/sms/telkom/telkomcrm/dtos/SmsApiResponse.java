package com.sms.telkom.telkomcrm.dtos;

import lombok.Data;

@Data
public class SmsApiResponse implements ResponseInterface {


    private int status;

    private String trxId;

    private String statusDesc;

    private String gatewayName;
}
