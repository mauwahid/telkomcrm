package com.sms.telkom.telkomcrm.dtos;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class ExcelObject {

    private String title;
    private String dateStart;
    private String dateEnd;

}
