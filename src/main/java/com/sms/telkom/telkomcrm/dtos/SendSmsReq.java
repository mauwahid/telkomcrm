package com.sms.telkom.telkomcrm.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SendSmsReq {

    private String username;

    private String password;

    private String content;

    private String senderId;

    private String msisdn;

    private String ticketNum;

    private int type;
}
