package com.sms.telkom.telkomcrm.dtos;

import lombok.Data;

@Data
public class SendSmsRes implements ResponseInterface {

    private int status;
    private String statusDesc;
    private int smsCount;
    private String operatorName;

}
