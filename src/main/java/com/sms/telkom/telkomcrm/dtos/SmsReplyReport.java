package com.sms.telkom.telkomcrm.dtos;

import lombok.Data;

@Data
public class SmsReplyReport {

    private String shortCode;

    private int trxY;

    private int sumSmsY;

    private int trxN;

    private int sumSmsN;

    private int trxFailed;

    private int sumSmsFailed;



}
