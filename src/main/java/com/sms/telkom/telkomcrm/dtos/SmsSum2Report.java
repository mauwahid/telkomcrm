package com.sms.telkom.telkomcrm.dtos;

import lombok.Data;

@Data
public class SmsSum2Report {

    private String operator;

    private String senderId;

    private int resolved;

    private int labAssign;

    private int smsOpen;

    private int smsGamasOpen;

    private int smsGamasClose;

    private int smsOthers;

    private int resolvedTrx;

    private int labAssignTrx;

    private int smsOpenTrx;

    private int smsGamasOpenTrx;

    private int smsGamasCloseTrx;

    private int smsOthersTrx;
}
