package com.sms.telkom.telkomcrm.dtos;

import lombok.Data;

@Data
public class SmsSumReport {

    private String operator;

    private int resolved;

    private int labAssign;

    private int smsOpen;

    private int smsGamasOpen;

    private int smsGamasClose;

    private int smsOthers;


}
