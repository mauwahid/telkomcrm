package com.sms.telkom.telkomcrm.dtos;

import lombok.Data;

@Data
public class ReceiveSmsResponse implements ResponseInterface {

    private String responseString;

}
