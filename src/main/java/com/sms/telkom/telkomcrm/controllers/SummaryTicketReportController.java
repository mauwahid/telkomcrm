package com.sms.telkom.telkomcrm.controllers;

import com.sms.telkom.telkomcrm.dtos.ExcelObject;
import com.sms.telkom.telkomcrm.dtos.SmsTicketSum;
import com.sms.telkom.telkomcrm.exporter.SmsTicketExporter;
import com.sms.telkom.telkomcrm.jdbctemplate.SmsSumTicketJdbc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Arrays;

@Controller
@ApiIgnore
@Slf4j
public class SummaryTicketReportController {


    @Autowired
    private SmsSumTicketJdbc smsTodayTicketJdbc;

    @Autowired
    private ExcelObject excelObject;

    @Autowired
    private SmsTicketExporter smsTicketExporter;


    @GetMapping("/summary/ticket")
    public String ticket(){
        return "summary/ticket";
    }

    @GetMapping("/summary/ticket/api")
    public @ResponseBody
    SmsTicketSum ticketApi(@RequestParam(name = "date_range")String dateRange){

      //  log.debug("dateRange : "+dateRange);
        String dateStart = dateRange.substring(0,10);
        String dateEnd = dateRange.substring(13, 23);

      //  log.debug("start "+dateStart);
      //  log.debug("end "+dateEnd);

        return smsTodayTicketJdbc.findRangeDate(dateStart, dateEnd);
    }

    @GetMapping("/summary/ticket/download")
    public ModelAndView downloadTicketSum(@RequestParam(name = "date_range")String dateRange){

        String dateStart = dateRange.substring(0,10);
        String dateEnd = dateRange.substring(13, 23);

        excelObject.setTitle("Ticket Summary");
        excelObject.setDateStart(dateStart);
        excelObject.setDateEnd(dateEnd);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setView(smsTicketExporter);
        modelAndView.addObject("excelObject", excelObject);
        modelAndView.addObject("smsTicketSums", Arrays.asList(smsTodayTicketJdbc.findRangeDate(dateStart, dateEnd)) );

        return modelAndView;
    }




}
