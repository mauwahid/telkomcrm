package com.sms.telkom.telkomcrm.controllers;

import com.sms.telkom.telkomcrm.dtos.ExcelObject;
import com.sms.telkom.telkomcrm.exporter.SmsSumExporter;
import com.sms.telkom.telkomcrm.jdbctemplate.SmsSum2SuccessReportJdbc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@Controller
@ApiIgnore
public class SummarySmsReportController {



    @Autowired
    private SmsSum2SuccessReportJdbc smsSum2SuccessReportJdbc;

    @Autowired
    private ExcelObject excelObject;

    @Autowired
    private SmsSumExporter smsSumExporter;




    @GetMapping("/summary/sms")
    public String sms(){
        return "summary/sms";
    }



    @GetMapping("/summary/sms/api")
    public @ResponseBody
    List ticketApi(@RequestParam(name = "date_range")String dateRange){

        String dateStart = dateRange.substring(0,10);
        String dateEnd = dateRange.substring(13, 23);


        return smsSum2SuccessReportJdbc.getAllOperator(dateStart, dateEnd);
    }

    @GetMapping("/summary/sms/download")
    public ModelAndView download(@RequestParam(name = "date_range")String dateRange){

        String dateStart = dateRange.substring(0,10);
        String dateEnd = dateRange.substring(13, 23);

        excelObject.setTitle("Sms Success Summary");
        excelObject.setDateStart(dateStart);
        excelObject.setDateEnd(dateEnd);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setView(smsSumExporter);
        modelAndView.addObject("excelObject", excelObject);
        modelAndView.addObject("smsSum2Reports", smsSum2SuccessReportJdbc.getAllOperator(dateStart, dateEnd) );

        return modelAndView;
    }






}
