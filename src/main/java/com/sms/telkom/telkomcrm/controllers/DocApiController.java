package com.sms.telkom.telkomcrm.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.view.RedirectView;
import springfox.documentation.annotations.ApiIgnore;

@Controller
@ApiIgnore
public class DocApiController {

    @GetMapping("/docapi")
    public String docApi(){
        return "docapi";
    }


}
