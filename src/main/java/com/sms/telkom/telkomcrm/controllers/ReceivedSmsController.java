package com.sms.telkom.telkomcrm.controllers;

import com.sms.telkom.telkomcrm.dtos.ReceiveSmsResponse;
import com.sms.telkom.telkomcrm.entities.MOFailed;
import com.sms.telkom.telkomcrm.repositories.MOFailedRepository;
import com.sms.telkom.telkomcrm.services.Receive1147Service;
import com.sms.telkom.telkomcrm.services.Receive2000Service;
import com.sms.telkom.telkomcrm.services.ReceiveSmsRoutingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.json.HTTP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Api(tags = "Receive Sms from Mobile (MO)")
@RequestMapping("/mo")
public class ReceivedSmsController {


    @Autowired
    private Receive1147Service receive1147Service;

    @Autowired
    private Receive2000Service receive2000Service;

    @Autowired
    private MOFailedRepository moFailedRepository;



    @ApiOperation(value = "Mobile Origin SMS", response = String.class)
    @GetMapping("/1147")
    public ResponseEntity receiveSms(@RequestParam("enmsisdn")String encodedMsisdn,
                                     @RequestParam("content")String content,
                                     @RequestParam("shortcode")String shortcode,
                                     @RequestParam("telco")String telco,
                                     @RequestParam("trxid")String trxId){

        ReceiveSmsResponse receiveSmsResponse = null;

        if(shortcode.equalsIgnoreCase("1147")){
            receiveSmsResponse = receive1147Service.smsprocessResponse(encodedMsisdn, shortcode, content, telco, trxId);
        }else{

            saveMoFailed(encodedMsisdn, content, shortcode, "INVALID DATA", trxId);
            receiveSmsResponse = new ReceiveSmsResponse();
            receiveSmsResponse.setResponseString("INVALID DATA");
        }


        return new ResponseEntity(receiveSmsResponse.getResponseString(), HttpStatus.OK);
    }

    @ApiOperation(value = "Mobile Origin SMS", response = String.class)
    @GetMapping("/2000")
    public ResponseEntity receive2000(@RequestParam("enmsisdn")String encodedMsisdn,
                                     @RequestParam("content")String content,
                                     @RequestParam("shortcode")String shortcode,
                                      @RequestParam("telco")String telco,
                                      @RequestParam("trxid")String trxId){

        ReceiveSmsResponse receiveSmsResponse = null;

        if(shortcode.equalsIgnoreCase("2000")){
            receiveSmsResponse = receive2000Service.smsprocessResponse(encodedMsisdn, shortcode, content, telco, trxId );
        }else{

            saveMoFailed(encodedMsisdn, content, shortcode, "INVALID DATA", trxId);
            receiveSmsResponse = new ReceiveSmsResponse();
            receiveSmsResponse.setResponseString("INVALID DATA");
        }


        return new ResponseEntity(receiveSmsResponse.getResponseString(), HttpStatus.OK);
    }


    private void saveMoFailed(String encodeMsisdn, String content, String shortCode, String response, String trxId){

        MOFailed moFailed = new MOFailed();
        moFailed.setContent(content);
        moFailed.setEncryptedMsisdn(encodeMsisdn);
        moFailed.setShortCode(shortCode);
        moFailed.setReplyToCustomer(response);
        moFailed.setTrxId(trxId);

        moFailedRepository.save(moFailed);
    }
}
