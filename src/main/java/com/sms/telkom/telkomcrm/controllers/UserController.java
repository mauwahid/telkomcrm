package com.sms.telkom.telkomcrm.controllers;

import com.sms.telkom.telkomcrm.entities.User;
import com.sms.telkom.telkomcrm.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

@Controller
@ApiIgnore
@Slf4j
public class UserController {

    @Autowired
    private UserRepository userRepository;


    @Autowired
    private PasswordEncoder passwordEncoder;


    @GetMapping("/users/list")
    public ModelMap userList(@PageableDefault Pageable pageable,
                             @RequestParam(name = "value", required = false) String value, Model model){

        if(value != null){
            model.addAttribute("key", value);
            return new ModelMap().addAttribute("userList", userRepository.findByUsernameContainingIgnoreCase(value, pageable));

        }else{
            return new ModelMap().addAttribute("userList", userRepository.findAll(pageable));
        }

    }

    @GetMapping("/users/form")
    public ModelMap tampilkanForm(@RequestParam(value = "id", required = false) User user, Model model) {


        if (user == null) {
            user = new User();
        }
        return new ModelMap("user", user);
    }

    @PostMapping("/users/form")
    public String simpan(@ModelAttribute @Valid User user, BindingResult err, SessionStatus status, Model model) {

     /*   if (!user.getUsername().isEmpty() && userRepository.isUsernameExist(user.getUsername())){
            if(userService.findByIdAndUsername(user.getId(),user.getUsername())==null)
                err.rejectValue("username", "error.username", "Username exists!");
        }
*/
        if(user.getId()==null && user.getPassword().isEmpty() && user.getPassword().length()<6)
            err.rejectValue("password", "error.password", "Password must be filled in & more than 5 chars!");

    //    log.debug("user Id "+user.getId());
        User user1 = null;
        if(user.getId()!=null)
            user1 = userRepository.findOne(user.getId());

        if(user1!=null)
            log.debug("user equal user1? "+user.equals(user1));

        if (err.hasErrors()) {
            return "users/form";
        }

        String errorRetype = null;



        if(!user.getPassword().equalsIgnoreCase("")){

            if(!user.getPassword().contentEquals(user.getRetypePassword())){
                errorRetype = "Password didn't match";
                model.addAttribute("errorRetype", errorRetype);
                return "users/form";
            }else {
                user.setEncryptedPassword(passwordEncoder.encode(user.getPassword()));
            }

        }

        userRepository.save(user);
        status.setComplete();
        return "redirect:/users/list";
    }


    @GetMapping("/users/delete")
    public Object delete(@RequestParam(value = "id", required = true) User user, SessionStatus status) {
        try{
            userRepository.delete(user);
        } catch (DataIntegrityViolationException exception) {
            status.setComplete();
            return new ModelAndView("error/errorHapus")
                    .addObject("entityId", user.getId())
                    .addObject("entityName", "user")
                    .addObject("errorCause", exception.getRootCause().getMessage())
                    .addObject("backLink","/client/user/list");
        }
        status.setComplete();
        return "redirect:/users/list";
    }
}
