package com.sms.telkom.telkomcrm.controllers;

import com.sms.telkom.telkomcrm.entities.SmsResolvedActive;
import com.sms.telkom.telkomcrm.entities.SmsResolvedArchive;
import com.sms.telkom.telkomcrm.repositories.SmsResolvedActiveRepository;
import com.sms.telkom.telkomcrm.repositories.SmsResolvedArchiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import springfox.documentation.annotations.ApiIgnore;

@Controller
@ApiIgnore
public class SmsArchiveController {

    @Autowired
    private SmsResolvedArchiveRepository smsResolvedArchiveRepository;


    private static final int PAGESIZE = 10;

    @GetMapping("/report/archive")
    public ModelMap list(@PageableDefault Pageable pageable,
                         @RequestParam(name="q", required = false) String query,
                         @RequestParam(name = "value", required = false) String value, Model model){

        PageRequest pageRequest = new PageRequest(pageable.getPageNumber(), PAGESIZE, Sort.Direction.DESC, "id");

        if(value != null && query != null){
            model.addAttribute("key", value);
            model.addAttribute("q", query );
            return new ModelMap().addAttribute("smsArchives", getQueryData(pageRequest, query, value));

        }else{
            return new ModelMap().addAttribute("smsArchives", smsResolvedArchiveRepository.findAll(pageRequest));
        }

    }


    private Page<SmsResolvedArchive> getQueryData(Pageable pageable, String query, String value){

        Page<SmsResolvedArchive> result = null;
        switch (query){
            case "msisdn" :
                result = smsResolvedArchiveRepository.findByMsisdnContaining(value, pageable);
                break;
            case "contentToCustomer" :
                result = smsResolvedArchiveRepository.findByContentToCustomerContaining(value, pageable);
                break;
            case "ticketNum":
                result = smsResolvedArchiveRepository.findByTicketNumContaining(value, pageable);
                break;

        }
        return result;
    }

}
