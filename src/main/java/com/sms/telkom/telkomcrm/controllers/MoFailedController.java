package com.sms.telkom.telkomcrm.controllers;

import com.sms.telkom.telkomcrm.entities.MOFailed;
import com.sms.telkom.telkomcrm.entities.MOSuccess;
import com.sms.telkom.telkomcrm.repositories.MOFailedRepository;
import com.sms.telkom.telkomcrm.repositories.MOSuccesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import springfox.documentation.annotations.ApiIgnore;

@Controller
@ApiIgnore
public class MoFailedController {

    @Autowired
    private MOFailedRepository moFailedRepository;


    private static final int PAGESIZE = 10;

    @GetMapping("/report/mofailed")
    public ModelMap list(@PageableDefault Pageable pageable,
                         @RequestParam(name="q", required = false) String query,
                         @RequestParam(name = "value", required = false) String value, Model model){

        PageRequest pageRequest = new PageRequest(pageable.getPageNumber(), PAGESIZE, Sort.Direction.DESC, "id");

        if(value != null && query != null){
            model.addAttribute("key", value);
            model.addAttribute("q", query );
            return new ModelMap().addAttribute("smsList", getQueryData(pageRequest, query, value));

        }else{
            return new ModelMap().addAttribute("smsList", moFailedRepository.findAll(pageRequest));
        }

    }


    private Page<MOFailed> getQueryData(Pageable pageable, String query, String value){

        Page<MOFailed> result = null;
        switch (query){
            case "content" :
                result = moFailedRepository.findByContentContainingIgnoreCase(value, pageable);
                break;
            }
        return result;
    }

}
