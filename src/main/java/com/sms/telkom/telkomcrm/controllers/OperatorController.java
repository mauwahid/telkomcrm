package com.sms.telkom.telkomcrm.controllers;

import com.sms.telkom.telkomcrm.entities.Operator;
import com.sms.telkom.telkomcrm.entities.User;
import com.sms.telkom.telkomcrm.repositories.OperatorRepository;
import com.sms.telkom.telkomcrm.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

@Controller
@ApiIgnore
public class OperatorController {

    @Autowired
    private OperatorRepository operatorRepository;


    @GetMapping("/operator/list")
    public ModelMap userList(@PageableDefault Pageable pageable,
                             @RequestParam(name = "value", required = false) String value, Model model){

        if(value != null){
            model.addAttribute("key", value);
            return new ModelMap().addAttribute("operatorList", operatorRepository.findByPrefix(value));

        }else{
            return new ModelMap().addAttribute("operatorList", operatorRepository.findAll(pageable));
        }

    }

    @GetMapping("/operator/form")
    public ModelMap tampilkanForm(@RequestParam(value = "id", required = false) Operator operator, Model model) {


        if (operator == null) {
            operator = new Operator();
        }
        return new ModelMap("operator", operator);
    }

    @PostMapping("/operator/form")
    public String simpan(@ModelAttribute @Valid Operator operator, BindingResult err, SessionStatus status, Model model) {


        if (err.hasErrors()) {
            return "operator/form";
        }


        operatorRepository.save(operator);
        status.setComplete();
        return "redirect:/operator/list";
    }


    @GetMapping("/operator/delete")
    public Object delete(@RequestParam(value = "id", required = true) Operator operator, SessionStatus status) {
        try{
            operatorRepository.delete(operator);
        } catch (DataIntegrityViolationException exception) {
            status.setComplete();
            return new ModelAndView("error/errorHapus")
                    .addObject("entityId", operator.getId())
                    .addObject("entityName", "user")
                    .addObject("errorCause", exception.getRootCause().getMessage())
                    .addObject("backLink","/operator/list");
        }
        status.setComplete();
        return "redirect:/operator/list";
    }
}
