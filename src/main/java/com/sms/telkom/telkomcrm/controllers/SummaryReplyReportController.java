package com.sms.telkom.telkomcrm.controllers;

import com.sms.telkom.telkomcrm.dtos.ExcelObject;
import com.sms.telkom.telkomcrm.exporter.SmsReplyExporter;
import com.sms.telkom.telkomcrm.jdbctemplate.SmsReplyReportJdbc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@Controller
@ApiIgnore
public class SummaryReplyReportController {


    @Autowired
    private SmsReplyReportJdbc smsReplyReportJdbc;

    @Autowired
    private ExcelObject excelObject;

    @Autowired
    private SmsReplyExporter smsReplyExporter;


    @GetMapping("/summary/reply")
    public String reply(){
        return "summary/reply";
    }


    @GetMapping("/summary/reply/api")
    public @ResponseBody
    List replyApi(@RequestParam(name = "date_range")String dateRange){

        String dateStart = dateRange.substring(0,10);
        String dateEnd = dateRange.substring(13, 23);


        return smsReplyReportJdbc.getAllData(dateStart, dateEnd);
    }

    @GetMapping("/summary/reply/download")
    public ModelAndView download(@RequestParam(name = "date_range")String dateRange){

        String dateStart = dateRange.substring(0,10);
        String dateEnd = dateRange.substring(13, 23);

        excelObject.setTitle("Sms Reply Summary");
        excelObject.setDateStart(dateStart);
        excelObject.setDateEnd(dateEnd);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setView(smsReplyExporter);
        modelAndView.addObject("excelObject", excelObject);
        modelAndView.addObject("smsSum2Reports",smsReplyReportJdbc.getAllData(dateStart, dateEnd) );

        return modelAndView;
    }


}
