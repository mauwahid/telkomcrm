package com.sms.telkom.telkomcrm.controllers;

import com.sms.telkom.telkomcrm.dtos.SendSmsReq;
import com.sms.telkom.telkomcrm.dtos.SendSmsRes;
import com.sms.telkom.telkomcrm.entities.User;
import com.sms.telkom.telkomcrm.repositories.UserRepository;
import com.sms.telkom.telkomcrm.services.SendSmsRoutingService;
import com.sms.telkom.telkomcrm.utils.Common;
import com.sms.telkom.telkomcrm.utils.SmsResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.servlet.http.HttpServletRequest;

@Controller
@Api(tags = "SMS Sending")
public class SendSmsController {

    @Autowired
    private SendSmsRoutingService sendSmsRoutingService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @ApiOperation(value = "SMS Sending API", response = SendSmsRes.class)
    @PostMapping("/send")
    public ResponseEntity send(HttpServletRequest request, @RequestBody SendSmsReq sendSmsReq){

        //long userId = authService.authKey(apiKey, ApiStatic.API_FLIGHT, ApiStatic.API_FLIGHT_BOOK, flightBookReq.toString().toString(), request.getRemoteAddr());

        User user = userRepository.findByUsernameAndPassword(sendSmsReq.getUsername(), sendSmsReq.getPassword());
      //  User user = userRepository.findByUsernameAndEncryptedPassword(sendSmsReq.getUsername(), passwordEncoder.encode(sendSmsReq.getPassword()));
        SendSmsRes  sendSmsRes = null;


        if(user==null){
            sendSmsRes = new SendSmsRes();
            sendSmsRes.setStatus(Common.STATUS_AUTH_FAILED);
            sendSmsRes.setStatusDesc(Common.STATUS_FAILED_DESC_AUTH);
        }else{
            Long idUser = user.getId();
            sendSmsRes = sendSmsRoutingService.composeSms(sendSmsReq, idUser);

        }


        return  new ResponseEntity(sendSmsRes, HttpStatus.OK);
    }

}
