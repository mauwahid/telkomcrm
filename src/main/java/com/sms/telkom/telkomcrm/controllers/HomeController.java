package com.sms.telkom.telkomcrm.controllers;

import com.sms.telkom.telkomcrm.dtos.ExcelObject;
import com.sms.telkom.telkomcrm.dtos.SmsTicketSum;
import com.sms.telkom.telkomcrm.exporter.SmsSumSimpleExporter;
import com.sms.telkom.telkomcrm.exporter.SmsTicketExporter;
import com.sms.telkom.telkomcrm.jdbctemplate.SmsSumReportJdbc;
import com.sms.telkom.telkomcrm.jdbctemplate.SmsTodayReportJdbc;
import com.sms.telkom.telkomcrm.jdbctemplate.SmsSumTicketJdbc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

import static java.util.Arrays.asList;

@Controller
@ApiIgnore
public class HomeController {


    @Autowired
    private SmsSumTicketJdbc smsTodayTicketJdbc;

    @Autowired
    private SmsTodayReportJdbc smsTodayReportJdbc;

    @Autowired
    private SmsSumReportJdbc smsSumReportJdbc;


    @Autowired
    private ExcelObject excelObject;

    @Autowired
    private SmsTicketExporter smsTicketExporter;

    @Autowired
    private SmsSumSimpleExporter smsSumSimpleExporter;


    @GetMapping("/")
    public String homepage(){
        return "dashboard";
    }

    @GetMapping("/dashboard")
    public String dashboard(){
        return "dashboard";
    }

    @GetMapping("/dashboard/ticket_sum")
    public @ResponseBody
    SmsTicketSum getSummaryTodayTicket(){

        return smsTodayTicketJdbc.findTodayData();
    }

    @GetMapping("/dashboard/today_report")
    public @ResponseBody
    List getTodayReport(){

        return smsTodayReportJdbc.getAllOperator();
    }

    @GetMapping("/dashboard/month_report")
    public @ResponseBody
    List getMonthReport(){

        return smsSumReportJdbc.getAllOperator();
    }

    // Download excel


    @GetMapping("/dashboard/ticket_sum/download")
    public ModelAndView downloadTicketSum(){

        excelObject.setTitle("Today Ticket Summary");
        excelObject.setDateStart("Today");

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setView(smsTicketExporter);
        modelAndView.addObject("excelObject", excelObject);
        modelAndView.addObject("smsTicketSums", asList(smsTodayTicketJdbc.findTodayData()));

        return modelAndView;
    }

    @GetMapping("/dashboard/today_report/download")
    public ModelAndView downloadTodaySum(){

        excelObject.setTitle("Today Sms Summary");
        excelObject.setDateStart("Today");

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setView(smsSumSimpleExporter);
        modelAndView.addObject("excelObject", excelObject);
        modelAndView.addObject("smsSumReports", smsTodayReportJdbc.getAllOperator());

        return modelAndView;
    }

    @GetMapping("/dashboard/month_report/download")
    public ModelAndView downloadMonthSum(){

        excelObject.setTitle("Month To Day Sms Summary");
        excelObject.setDateStart("Today");

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setView(smsSumSimpleExporter);
        modelAndView.addObject("excelObject", excelObject);
        modelAndView.addObject("smsSumReports", smsSumReportJdbc.getAllOperator());

        return modelAndView;
    }




}
