package com.sms.telkom.telkomcrm.rabbitmq;


import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;


@Component
@Slf4j
public class LogReceiver {


    @RabbitListener(queues = RabbitConfig.LOG_QUEUE)
    public void receive(Map<String, String> message){

        log.debug("Telkom Received : "+message.toString());

    }

}
