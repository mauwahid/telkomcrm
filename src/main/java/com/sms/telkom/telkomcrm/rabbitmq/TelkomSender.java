package com.sms.telkom.telkomcrm.rabbitmq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Slf4j
public class TelkomSender {

    @Autowired
    private RabbitTemplate rabbitTemplate;


    public void sendToTelkom(Map<String,String> messages){

        rabbitTemplate.convertAndSend(RabbitConfig.TELKOM_QUEUE, messages);

        log.debug("sent to telkom : "+messages);
    }

  /*  public void sendToTelkom(SmsActive smsTemp){


        rabbitTemplate.convertAndSend(RabbitConfig.TELKOM_QUEUE, smsTemp);

        log.debug("sent to telkom : "+smsTemp.toString());
    }
*/

}
