package com.sms.telkom.telkomcrm.rabbitmq;


import com.sms.telkom.telkomcrm.dtos.SmsApiResponse;
import com.sms.telkom.telkomcrm.entities.SmsResolvedActive;
import com.sms.telkom.telkomcrm.repositories.SmsResolvedActiveRepository;
import com.sms.telkom.telkomcrm.smsapi.TelkomApiService;
import com.sms.telkom.telkomcrm.utils.Common;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Slf4j
public class TelkomReceiver {


    @Autowired
    private TelkomApiService telkomApiService;

    @Autowired
    private SmsResolvedActiveRepository smsResolvedActiveRepository;


    @RabbitListener(queues = RabbitConfig.TELKOM_QUEUE)
    public void receive(Map<String, String> message){

        String msisdn = message.get("msisdn");
        String answer = message.get("answer");
        String ticketId = message.get("ticketNum");
        String id = message.get("id");

        log.debug("ticket "+ticketId+", ans "+answer);

        SmsApiResponse smsApiResponse = telkomApiService.sendSms(id, msisdn, ticketId, answer);
        saveData(ticketId,smsApiResponse );

    }



    private void saveData(String ticketId, SmsApiResponse apiResponse){

      //  log.debug("save "+ticketId);
      //  log.debug("api response "+apiResponse.getStatus());
      //  log.debug("res "+apiResponse.getStatusDesc());

        SmsResolvedActive smsResolvedActive = smsResolvedActiveRepository.findByTicketNum(ticketId);

        if(apiResponse.getStatus() == Common.STATUS_SUCCESS)
            smsResolvedActive.setSentToTelkomStatus(Common.STATUS_SUCCESS);
        else{
            smsResolvedActive.setSentToTelkomStatus(Common.STATUS_FAILED);
            smsResolvedActive.setSentToTelkomDesc(apiResponse.getStatusDesc());
        }
       // java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());

        smsResolvedActive.setIsSentToTelkom(1);
        smsResolvedActive.setSentToTelkomDate(new java.util.Date());
        smsResolvedActiveRepository.save(smsResolvedActive);



    }

/*
    @RabbitListener(queues = RabbitConfig.TELKOM_QUEUE)
    public void receive(SmsActive smsTemp){

        log.debug("Telkom Received : "+smsTemp.toString());

    }
*/
}
