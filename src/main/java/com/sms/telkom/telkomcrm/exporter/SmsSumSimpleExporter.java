package com.sms.telkom.telkomcrm.exporter;

import com.sms.telkom.telkomcrm.dtos.ExcelObject;
import com.sms.telkom.telkomcrm.dtos.SmsSumReport;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Component
public class SmsSumSimpleExporter extends AbstractXlsView {

    @Override
    protected void buildExcelDocument(Map<String, Object> model,
                                      Workbook workbook,
                                      HttpServletRequest request,
                                      HttpServletResponse response) throws Exception {

        String fileName = "sms_simple_sum_"+ UUID.randomUUID().toString()+".xls";

        // change the file name
        response.setHeader("Content-Disposition", "attachment; filename=\""+fileName+"\"");

        @SuppressWarnings("unchecked")
        List<SmsSumReport> smsSumReports = (List<SmsSumReport>) model.get("smsSumReports");

        @SuppressWarnings("unchecked")
        ExcelObject excelObject = (ExcelObject) model.get("excelObject");

        // create excel xls sheet
        Sheet sheet = workbook.createSheet(excelObject.getTitle());
        sheet.setDefaultColumnWidth(30);

        // create style for header cells
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        //    style.setFillForegroundColor(HSSFColor.BLUE.index);
        //    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setBold(true);
        font.setFontHeightInPoints((short)10);
        //    font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);


        // create header row
        Row title = sheet.createRow(0);
        title.createCell(0).setCellValue(excelObject.getTitle());
        title.getCell(0).setCellStyle(style);
        title.createCell(1).setCellValue("Date : ");
        title.getCell(1).setCellStyle(style);
        title.createCell(2).setCellValue(excelObject.getDateStart());
        title.getCell(2).setCellStyle(style);
        title.createCell(3).setCellValue("-");
        title.getCell(3).setCellStyle(style);
        title.createCell(4).setCellValue(excelObject.getDateEnd());
        title.getCell(4).setCellStyle(style);

        Row header = sheet.createRow(1);
        header.createCell(0).setCellValue("Operator");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("SMS Open");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("Lab Assign");
        header.getCell(2).setCellStyle(style);
        header.createCell(3).setCellValue("SMS Resolved");
        header.getCell(3).setCellStyle(style);
        header.createCell(4).setCellValue("Gamas Open");
        header.getCell(4).setCellStyle(style);
        header.createCell(5).setCellValue("Gamas Close");
        header.getCell(5).setCellStyle(style);
        header.createCell(6).setCellValue("Lain Lain");
        header.getCell(6).setCellStyle(style);
        header.createCell(7).setCellValue("Total");
        header.getCell(7).setCellStyle(style);

        int rowCount = 2;
        int total = 0;

        for(SmsSumReport smsSumReport : smsSumReports){
            Row row =  sheet.createRow(rowCount++);
            total = smsSumReport.getSmsOpen() + smsSumReport.getLabAssign() +
                    smsSumReport.getResolved() + smsSumReport.getSmsGamasOpen() +
                    smsSumReport.getSmsGamasClose() + smsSumReport.getSmsOthers();

            row.createCell(0).setCellValue(smsSumReport.getOperator());
            row.createCell(1).setCellValue(smsSumReport.getSmsOpen());
            row.createCell(2).setCellValue(smsSumReport.getLabAssign());
            row.createCell(3).setCellValue(smsSumReport.getResolved());
            row.createCell(4).setCellValue(smsSumReport.getSmsGamasOpen());
            row.createCell(5).setCellValue(smsSumReport.getSmsGamasClose());
            row.createCell(6).setCellValue(smsSumReport.getSmsOthers());
            row.createCell(7).setCellValue(total);
        }

    }

}
