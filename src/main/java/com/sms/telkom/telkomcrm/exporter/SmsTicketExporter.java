package com.sms.telkom.telkomcrm.exporter;

import com.sms.telkom.telkomcrm.dtos.ExcelObject;
import com.sms.telkom.telkomcrm.dtos.SmsTicketSum;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Component
public class SmsTicketExporter  extends AbstractXlsView{

    @Override
    protected void buildExcelDocument(Map<String, Object> model,
                                      Workbook workbook,
                                      HttpServletRequest request,
                                      HttpServletResponse response) throws Exception {

        String fileName = "sms_ticket_sum_"+UUID.randomUUID().toString()+".xls";

        // change the file name
        response.setHeader("Content-Disposition", "attachment; filename=\""+fileName+"\"");

        @SuppressWarnings("unchecked")
        List<SmsTicketSum> smsTicketSums = (List<SmsTicketSum>) model.get("smsTicketSums");

        @SuppressWarnings("unchecked")
        ExcelObject excelObject = (ExcelObject) model.get("excelObject");

        // create excel xls sheet
        Sheet sheet = workbook.createSheet(excelObject.getTitle());
        sheet.setDefaultColumnWidth(30);

        // create style for header cells
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
    //    style.setFillForegroundColor(HSSFColor.BLUE.index);
    //    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setBold(true);
        font.setFontHeightInPoints((short)10);
    //    font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);


        // create header row
        Row title = sheet.createRow(0);
        title.createCell(0).setCellValue(excelObject.getTitle());
        title.getCell(0).setCellStyle(style);
        title.createCell(1).setCellValue("Date : ");
        title.getCell(1).setCellStyle(style);
        title.createCell(2).setCellValue(excelObject.getDateStart());
        title.getCell(2).setCellStyle(style);
        title.createCell(3).setCellValue("-");
        title.getCell(3).setCellStyle(style);
        title.createCell(4).setCellValue(excelObject.getDateEnd());
        title.getCell(4).setCellStyle(style);

        Row header = sheet.createRow(1);
        header.createCell(0).setCellValue("Ticket");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("Answered Yes");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("Answered No");
        header.getCell(2).setCellStyle(style);
        header.createCell(3).setCellValue("No Answer");
        header.getCell(3).setCellStyle(style);



        int rowCount = 2;

        for(SmsTicketSum smsTicketSum : smsTicketSums){
            Row smsTicketRow =  sheet.createRow(rowCount++);
            smsTicketRow.createCell(0).setCellValue(smsTicketSum.getCountTicketNum());
            smsTicketRow.createCell(1).setCellValue(smsTicketSum.getAnsweredYes());
            smsTicketRow.createCell(2).setCellValue(smsTicketSum.getAnsweredNo());
            smsTicketRow.createCell(3).setCellValue(smsTicketSum.getNoResponse());

        }

    }

}
