package com.sms.telkom.telkomcrm.exporter;

import com.sms.telkom.telkomcrm.dtos.ExcelObject;
import com.sms.telkom.telkomcrm.dtos.SmsSum2Report;
import com.sms.telkom.telkomcrm.dtos.SmsSumReport;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Component
public class SmsSumExporter extends AbstractXlsView {

    @Override
    protected void buildExcelDocument(Map<String, Object> model,
                                      Workbook workbook,
                                      HttpServletRequest request,
                                      HttpServletResponse response) throws Exception {

        String fileName = "sms_sum_report_"+ UUID.randomUUID().toString()+".xls";

        // change the file name
        response.setHeader("Content-Disposition", "attachment; filename=\""+fileName+"\"");

        @SuppressWarnings("unchecked")
        List<SmsSum2Report> smsSumReports = (List<SmsSum2Report>) model.get("smsSum2Reports");

        @SuppressWarnings("unchecked")
        ExcelObject excelObject = (ExcelObject) model.get("excelObject");

        // create excel xls sheet
        Sheet sheet = workbook.createSheet(excelObject.getTitle());
        sheet.setDefaultColumnWidth(30);

        // create style for header cells
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        //    style.setFillForegroundColor(HSSFColor.BLUE.index);
        //    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setBold(true);
        font.setFontHeightInPoints((short)10);
        //    font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);


        // create header row
        Row title = sheet.createRow(0);
        title.createCell(0).setCellValue(excelObject.getTitle());
        title.getCell(0).setCellStyle(style);
        title.createCell(1).setCellValue("Date : ");
        title.getCell(1).setCellStyle(style);
        title.createCell(2).setCellValue(excelObject.getDateStart());
        title.getCell(2).setCellStyle(style);
        title.createCell(3).setCellValue("-");
        title.getCell(3).setCellStyle(style);
        title.createCell(4).setCellValue(excelObject.getDateEnd());
        title.getCell(4).setCellStyle(style);

        Row header = sheet.createRow(1);
        header.createCell(0).setCellValue("Operator");
        header.getCell(0).setCellStyle(style);
        sheet.addMergedRegion(new CellRangeAddress(1, 2, 0, 0));

        header.createCell(1).setCellValue("Sender ID");
        header.getCell(1).setCellStyle(style);
        sheet.addMergedRegion(new CellRangeAddress(1, 2, 1, 1));


        header.createCell(2).setCellValue("SMS Open");
        header.getCell(2).setCellStyle(style);
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 2, 3));

        header.createCell(4).setCellValue("Lab Assign");
        header.getCell(4).setCellStyle(style);
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 4, 5));

        header.createCell(6).setCellValue("SMS Resolved");
        header.getCell(6).setCellStyle(style);
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 6, 7));

        header.createCell(8).setCellValue("Gamas Open");
        header.getCell(8).setCellStyle(style);
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 8, 9));

        header.createCell(10).setCellValue("Gamas Close");
        header.getCell(10).setCellStyle(style);
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 10, 11));


        header.createCell(12).setCellValue("Lain Lain");
        header.getCell(12).setCellStyle(style);
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 12, 13));


        header.createCell(14).setCellValue("Total");
        header.getCell(14).setCellStyle(style);
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 14, 15));


        Row subHeader = sheet.createRow(2);
        subHeader.createCell(2).setCellValue("Trx");
        subHeader.getCell(2).setCellStyle(style);
        subHeader.createCell(3).setCellValue("Count");
        subHeader.getCell(3).setCellStyle(style);

        subHeader.createCell(4).setCellValue("Trx");
        subHeader.getCell(4).setCellStyle(style);
        subHeader.createCell(5).setCellValue("Count");
        subHeader.getCell(5).setCellStyle(style);


        subHeader.createCell(6).setCellValue("Trx");
        subHeader.getCell(6).setCellStyle(style);
        subHeader.createCell(7).setCellValue("Count");
        subHeader.getCell(7).setCellStyle(style);


        subHeader.createCell(8).setCellValue("Trx");
        subHeader.getCell(8).setCellStyle(style);
        subHeader.createCell(9).setCellValue("Count");
        subHeader.getCell(9).setCellStyle(style);


        subHeader.createCell(10).setCellValue("Trx");
        subHeader.getCell(10).setCellStyle(style);
        subHeader.createCell(11).setCellValue("Count");
        subHeader.getCell(11).setCellStyle(style);


        subHeader.createCell(12).setCellValue("Trx");
        subHeader.getCell(12).setCellStyle(style);
        subHeader.createCell(13).setCellValue("Count");
        subHeader.getCell(13).setCellStyle(style);

        subHeader.createCell(14).setCellValue("Trx");
        subHeader.getCell(14).setCellStyle(style);
        subHeader.createCell(15).setCellValue("Count");
        subHeader.getCell(15).setCellStyle(style);





        int rowCount = 3;
        int total = 0;
        int totalTrx = 0;

        for(SmsSum2Report smsSumReport : smsSumReports){
            Row row =  sheet.createRow(rowCount++);
            total = smsSumReport.getSmsOpen() + smsSumReport.getLabAssign() +
                    smsSumReport.getResolved() + smsSumReport.getSmsGamasOpen() +
                    smsSumReport.getSmsGamasClose() + smsSumReport.getSmsOthers();

            totalTrx = smsSumReport.getSmsOpenTrx() + smsSumReport.getLabAssignTrx() +
                    smsSumReport.getResolvedTrx() + smsSumReport.getSmsGamasOpenTrx() +
                    smsSumReport.getSmsGamasCloseTrx() + smsSumReport.getSmsOthersTrx();

            row.createCell(0).setCellValue(smsSumReport.getOperator());
            row.createCell(1).setCellValue(smsSumReport.getSenderId());

            row.createCell(2).setCellValue(smsSumReport.getSmsOpenTrx());
            row.createCell(3).setCellValue(smsSumReport.getSmsOpen());


            row.createCell(4).setCellValue(smsSumReport.getLabAssignTrx());
            row.createCell(5).setCellValue(smsSumReport.getLabAssign());


            row.createCell(6).setCellValue(smsSumReport.getResolvedTrx());
            row.createCell(7).setCellValue(smsSumReport.getResolved());


            row.createCell(8).setCellValue(smsSumReport.getSmsGamasOpenTrx());
            row.createCell(9).setCellValue(smsSumReport.getSmsGamasOpen());

            row.createCell(10).setCellValue(smsSumReport.getSmsGamasCloseTrx());
            row.createCell(11).setCellValue(smsSumReport.getSmsGamasClose());

            row.createCell(12).setCellValue(smsSumReport.getSmsOthersTrx());
            row.createCell(13).setCellValue(smsSumReport.getSmsOthers());

            row.createCell(14).setCellValue(totalTrx);
            row.createCell(15).setCellValue(total);



        }

    }

}

