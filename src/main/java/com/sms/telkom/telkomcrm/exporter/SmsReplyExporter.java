package com.sms.telkom.telkomcrm.exporter;

import com.sms.telkom.telkomcrm.dtos.ExcelObject;
import com.sms.telkom.telkomcrm.dtos.SmsReplyReport;
import com.sms.telkom.telkomcrm.dtos.SmsSum2Report;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Component
public class SmsReplyExporter extends AbstractXlsView {

    @Override
    protected void buildExcelDocument(Map<String, Object> model,
                                      Workbook workbook,
                                      HttpServletRequest request,
                                      HttpServletResponse response) throws Exception {

        String fileName = "sms_reply_"+ UUID.randomUUID().toString()+".xls";

        // change the file name
        response.setHeader("Content-Disposition", "attachment; filename=\""+fileName+"\"");

        @SuppressWarnings("unchecked")
        List<SmsReplyReport> replyReports = (List<SmsReplyReport>) model.get("smsReplyReports");

        @SuppressWarnings("unchecked")
        ExcelObject excelObject = (ExcelObject) model.get("excelObject");

        // create excel xls sheet
        Sheet sheet = workbook.createSheet(excelObject.getTitle());
        sheet.setDefaultColumnWidth(30);

        // create style for header cells
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        //    style.setFillForegroundColor(HSSFColor.BLUE.index);
        //    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setBold(true);
        font.setFontHeightInPoints((short)10);
        //    font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);


        // create header row
        Row title = sheet.createRow(0);
        title.createCell(0).setCellValue(excelObject.getTitle());
        title.getCell(0).setCellStyle(style);
        title.createCell(1).setCellValue("Date : ");
        title.getCell(1).setCellStyle(style);
        title.createCell(2).setCellValue(excelObject.getDateStart());
        title.getCell(2).setCellStyle(style);
        title.createCell(3).setCellValue("-");
        title.getCell(3).setCellStyle(style);
        title.createCell(4).setCellValue(excelObject.getDateEnd());
        title.getCell(4).setCellStyle(style);

        Row header = sheet.createRow(1);
        header.createCell(0).setCellValue("Short Code");
        header.getCell(0).setCellStyle(style);
        sheet.addMergedRegion(new CellRangeAddress(1, 2, 0, 0));

        header.createCell(1).setCellValue("Answer Y");
        header.getCell(1).setCellStyle(style);
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 1, 2));

        header.createCell(3).setCellValue("Answer N");
        header.getCell(3).setCellStyle(style);
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 3, 4));

        header.createCell(5).setCellValue("Failed");
        header.getCell(5).setCellStyle(style);
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 5, 6));

        Row subHeader = sheet.createRow(2);
        subHeader.createCell(1).setCellValue("Trx");
        subHeader.getCell(1).setCellStyle(style);
        subHeader.createCell(2).setCellValue("Count");
        subHeader.getCell(2).setCellStyle(style);

        subHeader.createCell(3).setCellValue("Trx");
        subHeader.getCell(3).setCellStyle(style);
        subHeader.createCell(4).setCellValue("Count");
        subHeader.getCell(4).setCellStyle(style);


        subHeader.createCell(5).setCellValue("Trx");
        subHeader.getCell(5).setCellStyle(style);
        subHeader.createCell(6).setCellValue("Count");
        subHeader.getCell(6).setCellStyle(style);


        int rowCount = 3;

        for(SmsReplyReport replyReport : replyReports){
            Row row =  sheet.createRow(rowCount++);

            row.createCell(0).setCellValue(replyReport.getShortCode());

            row.createCell(1).setCellValue(replyReport.getTrxY());
            row.createCell(2).setCellValue(replyReport.getSumSmsY());

            row.createCell(3).setCellValue(replyReport.getTrxN());
            row.createCell(4).setCellValue(replyReport.getSumSmsN());

            row.createCell(5).setCellValue(replyReport.getTrxFailed());
            row.createCell(6).setCellValue(replyReport.getSumSmsFailed());

        }

    }

}
