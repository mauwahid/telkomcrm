package com.sms.telkom.telkomcrm.utils;

public class TypeUtils {


   // public static final String SALAM_SIMPATIK = "SALAM_SIMPATIK";


    public static final int SMS_OPEN = 1;
    public static final int SMS_LAB_ASSIGN = 2;
    public static final int SMS_RESOLVED = 3;
    public static final int SMS_GAMAS_OPEN = 4;
    public static final int SMS_GAMAS_CLOSE = 5;
    public static final int OTHERS = 0;



    public static final int RECEIVE_SMS_RESOLVED = 1;
    public static final int RECEIVE_HD_SMS = 2;
    public static final int RECEIVE_PROFILING = 3;

    //Salam Perdana
    public static final int RECEIVE_SMS_CF = 4;
    public static final int RECEIVE_SMS_CF_A = 5;
    public static final int RECEIVE_SMS_CF_B = 6;
    public static final int RECEIVE_SMS_CF_C = 7;

    public static final int RECEIVE_GL = 8;
    public static final int RECEIVE_SURVEY_1 = 9;
    public static final int RECEIVE_SURVEY_2 = 10;

    public static final int RECEIVE_OTHERS = 0;
    public static final int RECEIVE_INVALID_TICKET_FORMAT = 99;


}
