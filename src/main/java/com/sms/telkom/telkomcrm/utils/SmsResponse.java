package com.sms.telkom.telkomcrm.utils;

public class SmsResponse {


    public static final String SMS_SALAH = "Maaf, keyword anda salah. Untuk meresponse balas sms ini dengan Y/N {No Tiket}(GRATIS),CS:(021)147";
    public static final String SMS_SALAH_2000 = "Maaf, keyword anda salah. Untuk meresponse balas sms ini dengan TLKM {No Tiket} Y/N. (GRATIS),CS:(021)147";
    public static final String SMS_NO_TICKET_ID = "Mohon maaf nomor tiket tidak tersedia atau expired";
    public static final String SMS_YES_OPEN_CLOSE = "Selamat menggunakan layanan Telkom. Untuk percepatan penanganan gangguan, install Myindihome Android https://goo.gl/H7xzYk - IOS https://goo.gl/va3a1E";
    public static final String SMS_NO_OPEN_CLOSE = "Terimakasih atas konfirmasinya, mohon maaf bila belum terselesaikan, kami akan cek kembali dan perbaiki lebih lanjut";

}
