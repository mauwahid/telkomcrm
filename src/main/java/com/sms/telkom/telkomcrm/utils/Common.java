package com.sms.telkom.telkomcrm.utils;

public class Common {


    public static final int STATUS_SUCCESS = 1;

    public static final int STATUS_FAILED = 2;


    public static final int STATUS_AUTH_FAILED = 403;

    public static final String STATUS_SUCCESS_DESC = "SUCCESS";

    public static final String STATUS_FAILED_DESC_AUTH = "Authentication failed";

    public static final String STATUS_FAILED_DESC_TICKET_AVAILABLE = "Ticket is exist, please use other ticket numbers";

}
