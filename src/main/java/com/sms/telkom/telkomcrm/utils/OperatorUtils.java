package com.sms.telkom.telkomcrm.utils;

public class OperatorUtils {


    public static final String TELKOMSEL = "TSEL";
    public static final String INDOSAT = "ISAT";
    public static final String XL = "XL";
    public static final String THREE = "THRE";
    public static final String SMART = "SMART";
    public static final String OTHERS = "OTHERS";
}
