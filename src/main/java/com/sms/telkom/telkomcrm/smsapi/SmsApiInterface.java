package com.sms.telkom.telkomcrm.smsapi;

import com.sms.telkom.telkomcrm.dtos.SmsApiResponse;

public interface SmsApiInterface {

     SmsApiResponse sendSms(String content, String senderId, String msisdn);
}
