package com.sms.telkom.telkomcrm.smsapi;

import com.sms.telkom.telkomcrm.dtos.SmsApiResponse;
import com.sms.telkom.telkomcrm.utils.Common;
import lombok.extern.slf4j.Slf4j;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class NadyneApiService implements SmsApiInterface {


    private String url = "http://103.39.52.171:11080/smsnadyne/sms.php?";
    // private String username = "infom3d14";
    // private String password = "iniInF0ny4";
    private String username = "infomedia1";
    private String password = "info112233";
    private String sendDesc = "TELKOM_CRM";

    @Override
    public SmsApiResponse sendSms(String content, String senderId, String msisdn) {

        SmsApiResponse smsApiResponse = new SmsApiResponse();

        Map<String, String> params = new HashMap<>();
        params.put("username", username);
        params.put("password", password);
        params.put("sourceaddr", senderId);
        params.put("destinationaddr", formatMsisdn(msisdn));
        params.put("shortmessage", content);
        params.put("desc", sendDesc);

        String response = "";
        try{
            response = sendRequest(url, params);

            log.info("response "+response);

            if(response.toLowerCase().contains("sent")){
                smsApiResponse.setStatus(Common.STATUS_SUCCESS);
                smsApiResponse.setTrxId(getTrxId(response));
            }else {
                smsApiResponse.setStatus(Common.STATUS_FAILED);
                smsApiResponse.setStatusDesc(response);
            }



        }catch (Exception ex){
            response = ex.toString();
            smsApiResponse.setStatus(Common.STATUS_FAILED);
            smsApiResponse.setStatusDesc(response);

        }
        return smsApiResponse;
    }


    private  String sendRequest(String uri, Map<String, String> params)
            throws IOException {

        //  OkHttpClient client = new OkHttpClient();
        log.debug("URI "+uri.toString());
        // uri = uri+"lion/search/best_price";
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        OkHttpClient client = builder.build();

        HttpUrl.Builder urlBuilder = HttpUrl.parse(uri).newBuilder();

        params.forEach((k,v)->
                {
                    urlBuilder.addQueryParameter(k,v);

                }
        );

        String url = urlBuilder.build().toString();

        log.debug("url : "+url);

        Request request = new Request.Builder().
                url(url).build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    private String formatMsisdn(String msisdn){
        String result = msisdn;
        result = result.replaceAll("[^0-9]", "");

        if (result.startsWith("0")) {
            result = "62" + result.substring(1);
        }
        if (result.startsWith("8")) {
            result = "62" + result.substring(0);
        }
        return result;
    }

    private String getTrxId(String responseContent){
        String responseLower = responseContent.toLowerCase();

        String trxId = "";
     /*   int idxTrx = responseLower.indexOf("<trxid>");
        int idxEnd = responseLower.indexOf("</trxid>", idxTrx);
        trxId = responseLower.substring(idxTrx + 7, idxEnd);
*/

        String[] splitter = responseContent.split("\\|");

        for(int i = 0;i<splitter.length;i++){
            log.debug(i+" : "+splitter[i]);
        }

        if(splitter.length > 1){
            trxId = splitter[1];
        }
        return trxId;
    }

}
