package com.sms.telkom.telkomcrm.jdbctemplate;

import com.sms.telkom.telkomcrm.dtos.SmsTicketSum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class SmsSumTicketJdbc {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    private class SmsSumTicketMapper implements RowMapper<SmsTicketSum> {

        @Override
        public SmsTicketSum mapRow(ResultSet resultSet, int i) throws SQLException {

            SmsTicketSum smsTicketSum = new SmsTicketSum();
            smsTicketSum.setAnsweredYes(resultSet.getInt("answered_yes"));
            smsTicketSum.setAnsweredNo(resultSet.getInt("answered_no"));
            smsTicketSum.setCountTicketNum(resultSet.getInt("count_ticket_num"));
            smsTicketSum.setNoResponse(resultSet.getInt("no_response"));


            return smsTicketSum;
        }
    }

    public SmsTicketSum findTodayData(){


        return jdbcTemplate.queryForObject("select (select count(*) from sms_resolved_active where date(date_created) = curdate()) as count_ticket_num ,\n" +
                "(select count(*) from sms_resolved_active where answer like 'Y' and date(date_created) = curdate())  as answered_yes ,\n" +
                "(select count(*) from sms_resolved_active where answer like 'N' and date(date_created) = curdate())  as answered_no,\n" +
                "(select count(*) from sms_resolved_active where answer is null and date(date_created) = curdate())  as no_response\n",new SmsSumTicketMapper()
        );

    }

    public SmsTicketSum findRangeDate(String dateStart, String dateEnd){


        return jdbcTemplate.queryForObject("select (select sum(sms) from (\n" +
                "select count(*) as sms from sms_resolved_active where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"'\n" +
                "union all\n" +
                "select count(*) as sms from sms_resolved_archive where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"') as sms) as count_ticket_num ,\n" +
                "(select sum(sms) from (select count(*) as sms from sms_resolved_active where answer like 'Y' and date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"'\n" +
                "union all\n" +
                "select count(*) from sms_resolved_archive where answer like 'Y' and date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"') as sms)  as answered_yes ,\n" +
                "(select sum(sms) from (select count(*) as sms from sms_resolved_active where answer like 'N' and date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"'\n" +
                "union all\n" +
                "select count(*) as sms from sms_resolved_archive where answer like 'N' and date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"') as sms) as answered_no,\n" +
                "(select sum(sms) from (select count(*) as sms from sms_resolved_active where answer is null and date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"'\n" +
                "union all\n" +
                "select count(*)  as sms from sms_resolved_archive where answer is null and date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"') as sms) as no_response",new SmsSumTicketMapper()
        );

    }





}
