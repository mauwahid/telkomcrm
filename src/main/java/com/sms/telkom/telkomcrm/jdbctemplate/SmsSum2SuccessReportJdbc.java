package com.sms.telkom.telkomcrm.jdbctemplate;

import com.sms.telkom.telkomcrm.dtos.SmsSum2Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class SmsSum2SuccessReportJdbc {


    @Autowired
    private JdbcTemplate jdbcTemplate;


    private class SmsSum2ReportMapper implements RowMapper<SmsSum2Report> {

        @Override
        public SmsSum2Report mapRow(ResultSet resultSet, int i) throws SQLException {

            SmsSum2Report smsSum = new SmsSum2Report();
            smsSum.setOperator(resultSet.getString("operator"));
            smsSum.setSenderId(resultSet.getString("sender_id_to_cust"));
            smsSum.setResolved(resultSet.getInt("resolved"));
            smsSum.setLabAssign(resultSet.getInt("lab_assign"));
            smsSum.setSmsOpen(resultSet.getInt("sms_open"));
            smsSum.setSmsGamasOpen(resultSet.getInt("sms_gamas_open"));
            smsSum.setSmsGamasClose(resultSet.getInt("sms_gamas_close"));
            smsSum.setSmsOthers(resultSet.getInt("sms_others"));

            smsSum.setResolvedTrx(resultSet.getInt("resolved_trx"));
            smsSum.setLabAssignTrx(resultSet.getInt("lab_assign_trx"));
            smsSum.setSmsOpenTrx(resultSet.getInt("sms_open_trx"));
            smsSum.setSmsGamasOpenTrx(resultSet.getInt("sms_gamas_open_trx"));
            smsSum.setSmsGamasCloseTrx(resultSet.getInt("sms_gamas_close_trx"));
            smsSum.setSmsOthersTrx(resultSet.getInt("sms_others_trx"));


            return smsSum;
        }
    }


    public List getAllOperator(String startDate, String endDate){
        List listSmsSumReport = new ArrayList<SmsSum2Report>();
        listSmsSumReport.add(findTSEL1147(startDate, endDate));
        listSmsSumReport.add(findTSELT147(startDate, endDate));
        listSmsSumReport.add(findISAT(startDate, endDate));
        listSmsSumReport.add(findXL(startDate, endDate));
        listSmsSumReport.add(findTHREE(startDate, endDate));
        listSmsSumReport.add(findSMART(startDate, endDate));
        listSmsSumReport.add(findOTHER(startDate, endDate));

        return listSmsSumReport;

    }

    public SmsSum2Report findTSEL1147(String dateStart, String dateEnd){


        return jdbcTemplate.queryForObject("select 'TSEL' as operator, '1147' as sender_id_to_cust,\n" +
                "(select sum(data_count)\n" +
                "\t\tfrom ( select COALESCE(sum(sms_count), 0) as data_count from sms_resolved_active where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = '1147' and sent_status = 1 \n" +
                "\t\tunion select COALESCE(sum(sms_count), 0) as data_count from sms_resolved_archive where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = '1147' and sent_status = 1 \n" +
                ") as data_count) as resolved,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_lab_assign where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = '1147' and sent_status = 1 ) as lab_assign, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL'  and sender_id_to_cust = '1147' and sent_status = 1 ) as sms_open, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL'  and sender_id_to_cust = '1147' and sent_status = 1 ) as sms_gamas_open, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_close where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = '1147' and sent_status = 1 ) as sms_gamas_close, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_others where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = '1147' and sent_status = 1 ) as sms_others,\n" +
                "(select sum(data_count)\n" +
                "\t\tfrom ( select count(*) as data_count from sms_resolved_active where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = '1147' and sent_status = 1 \n" +
                "\t\tunion select count(*) as data_count from sms_resolved_archive where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = '1147' and sent_status = 1 \n" +
                ") as data_count) as resolved_trx,\n" +
                "(select  count(*) from sms_lab_assign where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = '1147' and sent_status = 1 ) as lab_assign_trx, \n" +
                "(select  count(*) from sms_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL'  and sender_id_to_cust = '1147' and sent_status = 1 ) as sms_open_trx, \n" +
                "(select  count(*) from sms_gamas_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL'  and sender_id_to_cust = '1147' and sent_status = 1 ) as sms_gamas_open_trx, \n" +
                "(select  count(*) from sms_gamas_close where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = '1147' and sent_status = 1 ) as sms_gamas_close_trx, \n" +
                "(select  count(*) from sms_others where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = '1147' and sent_status = 1 ) as sms_others_trx",new SmsSum2ReportMapper()
        );

    }


    public SmsSum2Report findTSELT147(String dateStart, String dateEnd){


        return jdbcTemplate.queryForObject("select 'TSEL' as operator, 'TELKOM147' as sender_id_to_cust,\n" +
                "(select sum(data_count)\n" +
                "\t\tfrom ( select COALESCE(sum(sms_count), 0) as data_count from sms_resolved_active where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = 'TELKOM147' and sent_status = 1\n" +
                "\t\tunion select COALESCE(sum(sms_count), 0) as data_count from sms_resolved_archive where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = 'TELKOM147' and sent_status = 1\n" +
                ") as data_count) as resolved,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_lab_assign where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = 'TELKOM147' and sent_status = 1) as lab_assign, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL'  and sender_id_to_cust = 'TELKOM147' and sent_status = 1) as sms_open, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL'  and sender_id_to_cust = 'TELKOM147' and sent_status = 1) as sms_gamas_open, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_close where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = 'TELKOM147' and sent_status = 1) as sms_gamas_close, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_others where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = 'TELKOM147' and sent_status = 1) as sms_others,\n" +
                "(select sum(data_count)\n" +
                "\t\tfrom ( select count(*) as data_count from sms_resolved_active where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = 'TELKOM147' and sent_status = 1\n" +
                "\t\tunion select count(*) as data_count from sms_resolved_archive where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = 'TELKOM147' and sent_status = 1\n" +
                ") as data_count) as resolved_trx,\n" +
                "(select  count(*) from sms_lab_assign where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = 'TELKOM147' and sent_status = 1) as lab_assign_trx, \n" +
                "(select  count(*) from sms_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL'  and sender_id_to_cust = 'TELKOM147' and sent_status = 1) as sms_open_trx, \n" +
                "(select  count(*) from sms_gamas_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL'  and sender_id_to_cust = 'TELKOM147' and sent_status = 1) as sms_gamas_open_trx, \n" +
                "(select  count(*) from sms_gamas_close where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = 'TELKOM147' and sent_status = 1) as sms_gamas_close_trx, \n" +
                "(select  count(*) from sms_others where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'TSEL' and sender_id_to_cust = 'TELKOM147' and sent_status = 1) as sms_others_trx",new SmsSum2ReportMapper()
        );

    }

    public SmsSum2Report findISAT(String dateStart, String dateEnd){


        return jdbcTemplate.queryForObject("select 'ISAT' as operator, 'TELKOM147' as sender_id_to_cust,\n" +
                "(select sum(data_count)\n" +
                "\t\tfrom ( select COALESCE(sum(sms_count), 0) as data_count from sms_resolved_active where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'ISAT' and sent_status = 1\n" +
                "\t\tunion select COALESCE(sum(sms_count), 0) as data_count from sms_resolved_archive where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'ISAT' and sent_status = 1\n" +
                ") as data_count) as resolved,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_lab_assign where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'ISAT' and sent_status = 1) as lab_assign, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'ISAT'  and sent_status = 1) as sms_open, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'ISAT'  and sent_status = 1) as sms_gamas_open, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_close where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'ISAT' and sent_status = 1) as sms_gamas_close, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_others where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'ISAT' and sent_status = 1) as sms_others,\n" +
                "(select sum(data_count)\n" +
                "\t\tfrom ( select count(*) as data_count from sms_resolved_active where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'ISAT' and sent_status = 1\n" +
                "\t\tunion select count(*) as data_count from sms_resolved_archive where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'ISAT' and sent_status = 1\n" +
                ") as data_count) as resolved_trx,\n" +
                "(select  count(*) from sms_lab_assign where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'ISAT' and sent_status = 1) as lab_assign_trx, \n" +
                "(select  count(*) from sms_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'ISAT'  and sent_status = 1) as sms_open_trx, \n" +
                "(select  count(*) from sms_gamas_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'ISAT'  and sent_status = 1) as sms_gamas_open_trx, \n" +
                "(select  count(*) from sms_gamas_close where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'ISAT' and sent_status = 1) as sms_gamas_close_trx, \n" +
                "(select  count(*) from sms_others where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'ISAT' and sent_status = 1) as sms_others_trx",new SmsSum2ReportMapper()
        );

    }

    public SmsSum2Report findXL(String dateStart, String dateEnd){


        return jdbcTemplate.queryForObject("select 'XL' as operator, 'TELKOM147' as sender_id_to_cust,\n" +
                "(select sum(data_count)\n" +
                "\t\tfrom ( select COALESCE(sum(sms_count), 0) as data_count from sms_resolved_active where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'XL' and sent_status = 1\n" +
                "\t\tunion select COALESCE(sum(sms_count), 0) as data_count from sms_resolved_archive where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'XL' and sent_status = 1\n" +
                ") as data_count) as resolved,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_lab_assign where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'XL' and sent_status = 1) as lab_assign, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'XL'  and sent_status = 1) as sms_open, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'XL'  and sent_status = 1) as sms_gamas_open, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_close where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'XL' and sent_status = 1) as sms_gamas_close, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_others where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'XL' and sent_status = 1) as sms_others,\n" +
                "(select sum(data_count)\n" +
                "\t\tfrom ( select count(*) as data_count from sms_resolved_active where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'XL' and sent_status = 1\n" +
                "\t\tunion select count(*) as data_count from sms_resolved_archive where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'XL' and sent_status = 1\n" +
                ") as data_count) as resolved_trx,\n" +
                "(select  count(*) from sms_lab_assign where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'XL' and sent_status = 1) as lab_assign_trx, \n" +
                "(select  count(*) from sms_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'XL'  and sent_status = 1) as sms_open_trx, \n" +
                "(select  count(*) from sms_gamas_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'XL'  and sent_status = 1) as sms_gamas_open_trx, \n" +
                "(select  count(*) from sms_gamas_close where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'XL' and sent_status = 1) as sms_gamas_close_trx, \n" +
                "(select  count(*) from sms_others where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'XL' and sent_status = 1) as sms_others_trx",new SmsSum2ReportMapper()
        );

    }

    public SmsSum2Report findSMART(String dateStart, String dateEnd){


        return jdbcTemplate.queryForObject("select 'SMART' as operator, 'TELKOM147' as sender_id_to_cust,\n" +
                "(select sum(data_count)\n" +
                "\t\tfrom ( select COALESCE(sum(sms_count), 0) as data_count from sms_resolved_active where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1\n" +
                "\t\tunion select COALESCE(sum(sms_count), 0) as data_count from sms_resolved_archive where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1\n" +
                ") as data_count) as resolved,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_lab_assign where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1) as lab_assign, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART'  and sent_status = 1) as sms_open, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART'  and sent_status = 1) as sms_gamas_open, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_close where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1) as sms_gamas_close, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_others where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1) as sms_others,\n" +
                "(select sum(data_count)\n" +
                "\t\tfrom ( select count(*) as data_count from sms_resolved_active where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1\n" +
                "\t\tunion select count(*) as data_count from sms_resolved_archive where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1\n" +
                ") as data_count) as resolved_trx,\n" +
                "(select  count(*) from sms_lab_assign where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1) as lab_assign_trx, \n" +
                "(select  count(*) from sms_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART'  and sent_status = 1) as sms_open_trx, \n" +
                "(select  count(*) from sms_gamas_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART'  and sent_status = 1) as sms_gamas_open_trx, \n" +
                "(select  count(*) from sms_gamas_close where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1) as sms_gamas_close_trx, \n" +
                "(select  count(*) from sms_others where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1) as sms_others_trx",new SmsSum2ReportMapper()
        );

    }

    public SmsSum2Report findTHREE(String dateStart, String dateEnd){


        return jdbcTemplate.queryForObject("select 'SMART' as operator, 'TELKOM147' as sender_id_to_cust,\n" +
                "(select sum(data_count)\n" +
                "\t\tfrom ( select COALESCE(sum(sms_count), 0) as data_count from sms_resolved_active where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1\n" +
                "\t\tunion select COALESCE(sum(sms_count), 0) as data_count from sms_resolved_archive where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1\n" +
                ") as data_count) as resolved,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_lab_assign where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1) as lab_assign, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART'  and sent_status = 1) as sms_open, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART'  and sent_status = 1) as sms_gamas_open, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_close where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1) as sms_gamas_close, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_others where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1) as sms_others,\n" +
                "(select sum(data_count)\n" +
                "\t\tfrom ( select count(*) as data_count from sms_resolved_active where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1\n" +
                "\t\tunion select count(*) as data_count from sms_resolved_archive where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1\n" +
                ") as data_count) as resolved_trx,\n" +
                "(select  count(*) from sms_lab_assign where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1) as lab_assign_trx, \n" +
                "(select  count(*) from sms_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART'  and sent_status = 1) as sms_open_trx, \n" +
                "(select  count(*) from sms_gamas_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART'  and sent_status = 1) as sms_gamas_open_trx, \n" +
                "(select  count(*) from sms_gamas_close where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1) as sms_gamas_close_trx, \n" +
                "(select  count(*) from sms_others where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'SMART' and sent_status = 1) as sms_others_trx",new SmsSum2ReportMapper()
        );

    }

    public SmsSum2Report findOTHER(String dateStart, String dateEnd){


        return jdbcTemplate.queryForObject("select 'OTHER' as operator, 'TELKOM147' as sender_id_to_cust,\n" +
                "(select sum(data_count)\n" +
                "\t\tfrom ( select COALESCE(sum(sms_count), 0) as data_count from sms_resolved_active where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'OTHER' and sent_status = 1\n" +
                "\t\tunion select COALESCE(sum(sms_count), 0) as data_count from sms_resolved_archive where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'OTHER' and sent_status = 1\n" +
                ") as data_count) as resolved,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_lab_assign where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'OTHER' and sent_status = 1) as lab_assign, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'OTHER'  and sent_status = 1) as sms_open, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'OTHER'  and sent_status = 1) as sms_gamas_open, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_close where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'OTHER' and sent_status = 1) as sms_gamas_close, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_others where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'OTHER' and sent_status = 1) as sms_others,\n" +
                "(select sum(data_count)\n" +
                "\t\tfrom ( select count(*) as data_count from sms_resolved_active where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'OTHER' and sent_status = 1\n" +
                "\t\tunion select count(*) as data_count from sms_resolved_archive where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'OTHER' and sent_status = 1\n" +
                ") as data_count) as resolved_trx,\n" +
                "(select  count(*) from sms_lab_assign where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'OTHER' and sent_status = 1) as lab_assign_trx, \n" +
                "(select  count(*) from sms_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'OTHER'  and sent_status = 1) as sms_open_trx, \n" +
                "(select  count(*) from sms_gamas_open where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'OTHER'  and sent_status = 1) as sms_gamas_open_trx, \n" +
                "(select  count(*) from sms_gamas_close where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'OTHER' and sent_status = 1) as sms_gamas_close_trx, \n" +
                "(select  count(*) from sms_others where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and operator = 'OTHER' and sent_status = 1) as sms_others_trx",new SmsSum2ReportMapper()
        );

    }


}
