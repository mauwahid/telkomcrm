package com.sms.telkom.telkomcrm.jdbctemplate;

import com.sms.telkom.telkomcrm.dtos.SmsSumReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class SmsTodayReportJdbc {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    private class SmsTodayReportMapper implements RowMapper<SmsSumReport>{

        @Override
        public SmsSumReport mapRow(ResultSet resultSet, int i) throws SQLException {

            SmsSumReport smsSumReport = new SmsSumReport();
            smsSumReport.setOperator(resultSet.getString("operator"));
            smsSumReport.setResolved(resultSet.getInt("resolved"));
            smsSumReport.setLabAssign(resultSet.getInt("lab_assign"));
            smsSumReport.setSmsOpen(resultSet.getInt("sms_open"));
            smsSumReport.setSmsGamasOpen(resultSet.getInt("sms_gamas_open"));
            smsSumReport.setSmsGamasClose(resultSet.getInt("sms_gamas_close"));
            smsSumReport.setSmsOthers(resultSet.getInt("sms_others"));


            return smsSumReport;
        }
    }

    public List getAllOperator(){
        List listSmsTodayReport = new ArrayList<SmsSumReport>();
        listSmsTodayReport.add(findTSELData());
        listSmsTodayReport.add(findISATData());
        listSmsTodayReport.add(findXLData());
        listSmsTodayReport.add(findSMARTData());
        listSmsTodayReport.add(findTHREEData());
        listSmsTodayReport.add(findOTHERData());

        return listSmsTodayReport;

    }

    public SmsSumReport findTSELData(){


        return jdbcTemplate.queryForObject("" +
                "select 'TSEL' as operator, \n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_resolved_active where date(date_created) = curdate() and operator = 'TSEL') as resolved,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_lab_assign where date(date_created) = curdate() and operator = 'TSEL') as lab_assign,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_open where date(date_created) = curdate() and operator = 'TSEL') as sms_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_open where date(date_created) = curdate() and operator = 'TSEL') as sms_gamas_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_close where date(date_created) = curdate() and operator = 'TSEL') as sms_gamas_close,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_others where date(date_created) = curdate() and operator = 'TSEL') as sms_others\n",new SmsTodayReportMapper()
        );

    }

    public SmsSumReport findISATData(){


        return jdbcTemplate.queryForObject("" +
                "select 'ISAT' as operator," +
                "(select  COALESCE(sum(sms_count), 0) from sms_resolved_active where date(date_created) = curdate() and operator = 'ISAT') as resolved,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_lab_assign where date(date_created) = curdate() and operator = 'ISAT') as lab_assign,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_open where date(date_created) = curdate() and operator = 'ISAT') as sms_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_open where date(date_created) = curdate() and operator = 'ISAT') as sms_gamas_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_close where date(date_created) = curdate() and operator = 'ISAT') as sms_gamas_close,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_others where date(date_created) = curdate() and operator = 'ISAT') as sms_others\n",new SmsTodayReportMapper()
        );

    }

    public SmsSumReport findXLData(){


        return jdbcTemplate.queryForObject("" +
                "select 'XL' as operator," +
                "(select  COALESCE(sum(sms_count), 0) from sms_resolved_active where date(date_created) = curdate() and operator = 'XL') as resolved,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_lab_assign where date(date_created) = curdate() and operator = 'XL') as lab_assign,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_open where date(date_created) = curdate() and operator = 'XL') as sms_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_open where date(date_created) = curdate() and operator = 'XL') as sms_gamas_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_close where date(date_created) = curdate() and operator = 'XL') as sms_gamas_close,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_others where date(date_created) = curdate() and operator = 'XL') as sms_others\n",new SmsTodayReportMapper()
        );

    }

    public SmsSumReport findSMARTData(){


        return jdbcTemplate.queryForObject("" +
                "select 'SMART' as operator," +
                "(select  COALESCE(sum(sms_count), 0) from sms_resolved_active where date(date_created) = curdate() and operator = 'SMART') as resolved,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_lab_assign where date(date_created) = curdate() and operator = 'SMART') as lab_assign,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_open where date(date_created) = curdate() and operator = 'SMART') as sms_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_open where date(date_created) = curdate() and operator = 'SMART') as sms_gamas_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_close where date(date_created) = curdate() and operator = 'SMART') as sms_gamas_close,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_others where date(date_created) = curdate() and operator = 'SMART') as sms_others\n",new SmsTodayReportMapper()
        );

    }

    public SmsSumReport findOTHERData(){


        return jdbcTemplate.queryForObject("" +
                "select 'OTHER' as operator," +
                "(select  COALESCE(sum(sms_count), 0) from sms_resolved_active where date(date_created) = curdate() and operator = 'OTHER') as resolved,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_lab_assign where date(date_created) = curdate() and operator = 'OTHER') as lab_assign,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_open where date(date_created) = curdate() and operator = 'OTHER') as sms_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_open where date(date_created) = curdate() and operator = 'OTHER') as sms_gamas_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_close where date(date_created) = curdate() and operator = 'OTHER') as sms_gamas_close,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_others where date(date_created) = curdate() and operator = 'OTHER') as sms_others\n",new SmsTodayReportMapper()
        );

    }

    public SmsSumReport findTHREEData(){


        return jdbcTemplate.queryForObject("" +
                "select 'THREE' as operator," +
                "(select  COALESCE(sum(sms_count), 0) from sms_resolved_active where date(date_created) = curdate() and operator = 'THREE') as resolved,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_lab_assign where date(date_created) = curdate() and operator = 'THREE') as lab_assign,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_open where date(date_created) = curdate() and operator = 'THREE') as sms_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_open where date(date_created) = curdate() and operator = 'THREE') as sms_gamas_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_close where date(date_created) = curdate() and operator = 'THREE') as sms_gamas_close,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_others where date(date_created) = curdate() and operator = 'THREE') as sms_others\n",new SmsTodayReportMapper()
        );

    }
}
