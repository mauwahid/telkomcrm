package com.sms.telkom.telkomcrm.jdbctemplate;

import com.sms.telkom.telkomcrm.dtos.SmsReplyReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class SmsReplyReportJdbc {


    @Autowired
    private JdbcTemplate jdbcTemplate;


    private class SmsReplyReportMapper implements RowMapper<SmsReplyReport> {

        @Override
        public SmsReplyReport mapRow(ResultSet resultSet, int i) throws SQLException {


            int y = resultSet.getInt("answer_y");
            int sumY = y * 2;

            int n = resultSet.getInt("answer_n");
            int sumN = n * 2;

            int failed = resultSet.getInt("failed");
            int sumFailed = failed * 2;


            SmsReplyReport smsReplyReport = new SmsReplyReport();
            smsReplyReport.setShortCode(resultSet.getString("short_code"));
            smsReplyReport.setTrxY(y);
            smsReplyReport.setSumSmsY(sumY);
            smsReplyReport.setTrxN(n);
            smsReplyReport.setSumSmsN(sumN);
            smsReplyReport.setTrxFailed(failed);
            smsReplyReport.setSumSmsFailed(sumFailed);

            return smsReplyReport;
        }
    }


    public List getAllData(String dateStart, String dateEnd){
        List list = new ArrayList<SmsReplyReport>();
        list.add(find1147(dateStart, dateEnd));
        list.add(find2000(dateStart, dateEnd));

        return list;
    }

    private SmsReplyReport find1147(String dateStart, String dateEnd){


        return jdbcTemplate.queryForObject("select '1147' as short_code, (select count(*) from mosuccess\n" +
                "where answer = 'Y' and date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and short_code = '1147'  ) as answer_y,\n" +
                "(select count(*) from mosuccess\n" +
                "where answer = 'N' and date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and short_code = '1147') as answer_n,\n" +
                "(select count(*) from mofailed\n" +
                "where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and short_code = '1147') as failed",new SmsReplyReportMapper()
        );

    }

    private SmsReplyReport find2000(String dateStart, String dateEnd){


        return jdbcTemplate.queryForObject("select '2000' as short_code, (select count(*) from mosuccess\n" +
                "where answer = 'Y' and date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and short_code = '2000') as answer_y,\n" +
                "(select count(*) from mosuccess\n" +
                "where answer = 'N' and date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and short_code = '2000') as answer_n,\n" +
                "(select count(*) from mofailed\n" +
                "where date(date_created) >= '"+dateStart+"' and date(date_created) <= '"+dateEnd+"' and short_code = '2000') as failed",new SmsReplyReportMapper()
        );

    }




}
