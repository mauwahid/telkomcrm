package com.sms.telkom.telkomcrm.jdbctemplate;

import com.sms.telkom.telkomcrm.dtos.SmsSumReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class SmsSumReportJdbc {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    private class SmsSumReportMapper implements RowMapper<SmsSumReport> {

        @Override
        public SmsSumReport mapRow(ResultSet resultSet, int i) throws SQLException {

            SmsSumReport smsmMonthReport = new SmsSumReport();
            smsmMonthReport.setOperator(resultSet.getString("operator"));
            smsmMonthReport.setResolved(resultSet.getInt("resolved"));
            smsmMonthReport.setLabAssign(resultSet.getInt("lab_assign"));
            smsmMonthReport.setSmsOpen(resultSet.getInt("sms_open"));
            smsmMonthReport.setSmsGamasOpen(resultSet.getInt("sms_gamas_open"));
            smsmMonthReport.setSmsGamasClose(resultSet.getInt("sms_gamas_close"));
            smsmMonthReport.setSmsOthers(resultSet.getInt("sms_others"));


            return smsmMonthReport;
        }
    }

    public List getAllOperator(){
        List listSmsSumReport = new ArrayList<SmsSumReport>();
        listSmsSumReport.add(findTSELData());
        listSmsSumReport.add(findISATData());
        listSmsSumReport.add(findXLData());
        listSmsSumReport.add(findSMARTData());
        listSmsSumReport.add(findTHREEData());
        listSmsSumReport.add(findOTHERData());

        return listSmsSumReport;

    }

    public SmsSumReport findTSELData(){


        return jdbcTemplate.queryForObject("" +
                "select 'TSEL' as operator, \n" +
                "(select sum(data) from (\n" +
                "                select  COALESCE(sum(sms_count), 0) as data from sms_resolved_active where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'TSEL'\n" +
                "                UNION\n" +
                "                select  COALESCE(sum(sms_count), 0) as data from sms_resolved_archive where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'TSEL'\n" +
                "                \n" +
                "                )as data)\n" +
                "                as resolved,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_lab_assign where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'TSEL') as lab_assign,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_open where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'TSEL') as sms_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_open where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'TSEL') as sms_gamas_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_close where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'TSEL') as sms_gamas_close,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_others where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'TSEL') as sms_others\n",new SmsSumReportMapper()
        );

    }

    public SmsSumReport findISATData(){


        return jdbcTemplate.queryForObject("" +
                "select 'ISAT' as operator," +
                "(select sum(data) from (\n" +
                "                select  COALESCE(sum(sms_count), 0) as data from sms_resolved_active where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'ISAT'\n" +
                "                UNION\n" +
                "                select  COALESCE(sum(sms_count), 0) as data from sms_resolved_archive where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'ISAT'\n" +
                "                \n" +
                "                )as data)\n" +
                "                as resolved,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_lab_assign where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'ISAT') as lab_assign,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_open where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'ISAT') as sms_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_open where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'ISAT') as sms_gamas_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_close where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'ISAT') as sms_gamas_close,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_others where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'ISAT') as sms_others\n",new SmsSumReportMapper()
        );

    }

    public SmsSumReport findXLData(){


        return jdbcTemplate.queryForObject("" +
                "select 'XL' as operator," +
                "(select sum(data) from (\n" +
                "                select  COALESCE(sum(sms_count), 0) as data from sms_resolved_active where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'XL'\n" +
                "                UNION\n" +
                "                select  COALESCE(sum(sms_count), 0) as data from sms_resolved_archive where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'XL'\n" +
                "                \n" +
                "                )as data)\n" +
                "                as resolved,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_lab_assign where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'XL') as lab_assign,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_open where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'XL') as sms_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_open where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'XL') as sms_gamas_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_close where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'XL') as sms_gamas_close,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_others where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'XL') as sms_others\n",new SmsSumReportMapper()
        );

    }

    public SmsSumReport findSMARTData(){


        return jdbcTemplate.queryForObject("" +
                "select 'SMART' as operator," +
                "(select sum(data) from (\n" +
                "                select  COALESCE(sum(sms_count), 0) as data from sms_resolved_active where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'SMART'\n" +
                "                UNION\n" +
                "                select  COALESCE(sum(sms_count), 0) as data from sms_resolved_archive where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'SMART'\n" +
                "                \n" +
                "                )as data)\n" +
                "                as resolved,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_lab_assign where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'SMART') as lab_assign,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_open where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'SMART') as sms_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_open where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'SMART') as sms_gamas_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_close where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'SMART') as sms_gamas_close,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_others where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'SMART') as sms_others\n",new SmsSumReportMapper()
        );

    }

    public SmsSumReport findOTHERData(){


        return jdbcTemplate.queryForObject("" +
                "select 'OTHER' as operator," +
                "(select sum(data) from (\n" +
                "                select  COALESCE(sum(sms_count), 0) as data from sms_resolved_active where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'OTHER'\n" +
                "                UNION\n" +
                "                select  COALESCE(sum(sms_count), 0) as data from sms_resolved_archive where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'OTHER'\n" +
                "                \n" +
                "                )as data)\n" +
                "                as resolved,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_lab_assign where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'OTHER') as lab_assign,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_open where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'OTHER') as sms_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_open where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'OTHER') as sms_gamas_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_close where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'OTHER') as sms_gamas_close,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_others where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'OTHER') as sms_others\n",new SmsSumReportMapper()
        );

    }

    public SmsSumReport findTHREEData(){


        return jdbcTemplate.queryForObject("" +
                "select 'THREE' as operator," +
                "(select sum(data) from (\n" +
                "                select  COALESCE(sum(sms_count), 0) as data from sms_resolved_active where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'THREE'\n" +
                "                UNION\n" +
                "                select  COALESCE(sum(sms_count), 0) as data from sms_resolved_archive where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'THREE'\n" +
                "                \n" +
                "                )as data)\n" +
                "                as resolved,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_lab_assign where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'THREE') as lab_assign,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_open where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'THREE') as sms_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_open where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'THREE') as sms_gamas_open,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_gamas_close where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'THREE') as sms_gamas_close,\n" +
                "(select  COALESCE(sum(sms_count), 0) from sms_others where (date_created between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) and operator = 'THREE') as sms_others\n",new SmsSumReportMapper()
        );

    }
}
