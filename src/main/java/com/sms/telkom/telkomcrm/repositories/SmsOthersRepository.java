package com.sms.telkom.telkomcrm.repositories;

import com.sms.telkom.telkomcrm.entities.SmsOpen;
import com.sms.telkom.telkomcrm.entities.SmsOthers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SmsOthersRepository extends PagingAndSortingRepository<SmsOthers, Long> {

    Page<SmsOthers> findByMsisdnContaining(String msisdn, Pageable pageable);

    Page<SmsOthers> findByContentToCustomerContaining(String content, Pageable pageable);
}
