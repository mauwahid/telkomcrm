package com.sms.telkom.telkomcrm.repositories;

import com.sms.telkom.telkomcrm.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    User findByUsernameAndPassword(String username, String password);

    Page<User> findByUsernameContainingIgnoreCase(String username, Pageable pageable);

    User findByUsernameAndEncryptedPassword(String username, String encryptedPassword);

}
