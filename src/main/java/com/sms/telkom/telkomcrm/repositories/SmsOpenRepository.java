package com.sms.telkom.telkomcrm.repositories;

import com.sms.telkom.telkomcrm.entities.SmsOpen;
import com.sms.telkom.telkomcrm.entities.SmsResolvedActive;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SmsOpenRepository extends PagingAndSortingRepository<SmsOpen, Long> {

    Page<SmsOpen> findByMsisdnContaining(String msisdn, Pageable pageable);

    Page<SmsOpen> findByContentToCustomerContaining(String content, Pageable pageable);
}
