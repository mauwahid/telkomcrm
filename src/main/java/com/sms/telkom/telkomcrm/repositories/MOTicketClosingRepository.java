package com.sms.telkom.telkomcrm.repositories;

import com.sms.telkom.telkomcrm.entities.MOTicketClosing;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MOTicketClosingRepository extends PagingAndSortingRepository<MOTicketClosing, Long> {
}
