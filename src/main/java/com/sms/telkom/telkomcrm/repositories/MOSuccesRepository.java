package com.sms.telkom.telkomcrm.repositories;

import com.sms.telkom.telkomcrm.entities.MOSuccess;
import com.sms.telkom.telkomcrm.entities.SmsResolvedActive;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MOSuccesRepository extends PagingAndSortingRepository<MOSuccess, Long> {


    Page<MOSuccess> findByTicketNumberContainingIgnoreCase(String ticketNum, Pageable pageable);

    Page<MOSuccess> findByMsisdnContainingIgnoreCase(String msisdn, Pageable pageable);

    Page<MOSuccess> findByContentContainingIgnoreCase(String content, Pageable pageable);
}
