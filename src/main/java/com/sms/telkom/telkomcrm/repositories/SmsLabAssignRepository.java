package com.sms.telkom.telkomcrm.repositories;

import com.sms.telkom.telkomcrm.entities.SmsLabAssign;
import com.sms.telkom.telkomcrm.entities.SmsOpen;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SmsLabAssignRepository extends PagingAndSortingRepository<SmsLabAssign, Long> {

    Page<SmsLabAssign> findByMsisdnContaining(String msisdn, Pageable pageable);

    Page<SmsLabAssign> findByContentToCustomerContaining(String content, Pageable pageable);
}
