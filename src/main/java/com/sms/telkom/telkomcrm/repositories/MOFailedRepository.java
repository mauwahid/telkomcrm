package com.sms.telkom.telkomcrm.repositories;

import com.sms.telkom.telkomcrm.entities.MOFailed;
import com.sms.telkom.telkomcrm.entities.MOSuccess;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MOFailedRepository extends PagingAndSortingRepository<MOFailed, Long> {

    Page<MOFailed> findByContentContainingIgnoreCase(String content, Pageable pageable);
}
