package com.sms.telkom.telkomcrm.repositories;

import com.sms.telkom.telkomcrm.entities.Operator;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OperatorRepository extends PagingAndSortingRepository<Operator, Long> {

    Operator findByPrefix(String prefix);
}
