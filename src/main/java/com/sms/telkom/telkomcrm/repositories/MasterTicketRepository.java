package com.sms.telkom.telkomcrm.repositories;

import com.sms.telkom.telkomcrm.entities.MasterTicket;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterTicketRepository extends CrudRepository<MasterTicket, Long> {

     MasterTicket findByTicketNum(String ticketId);
}
