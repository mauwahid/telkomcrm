package com.sms.telkom.telkomcrm.repositories;

import com.sms.telkom.telkomcrm.entities.SmsResolvedActive;
import com.sms.telkom.telkomcrm.entities.SmsResolvedArchive;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SmsResolvedArchiveRepository extends PagingAndSortingRepository<SmsResolvedArchive, Long> {

    SmsResolvedArchive findByTicketNum(String ticketNum);

    Page<SmsResolvedArchive> findByTicketNumContaining(String ticketNum, Pageable pageable);

    Page<SmsResolvedArchive> findByMsisdnContaining(String msisdn, Pageable pageable);

    Page<SmsResolvedArchive> findByContentToCustomerContaining(String content, Pageable pageable);
}
