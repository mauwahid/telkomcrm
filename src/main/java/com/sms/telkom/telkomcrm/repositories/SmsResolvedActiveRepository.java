package com.sms.telkom.telkomcrm.repositories;

import com.sms.telkom.telkomcrm.entities.SmsResolvedActive;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SmsResolvedActiveRepository extends PagingAndSortingRepository<SmsResolvedActive,Long> {

    SmsResolvedActive findByTicketNum(String ticketNum);

    Page<SmsResolvedActive> findByTicketNumContaining(String ticketNum, Pageable pageable);

    Page<SmsResolvedActive> findByMsisdnContaining(String msisdn, Pageable pageable);

    Page<SmsResolvedActive> findByContentToCustomerContainingIgnoreCase(String content, Pageable pageable);
}
