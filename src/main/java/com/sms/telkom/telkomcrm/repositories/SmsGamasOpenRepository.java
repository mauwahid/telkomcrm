package com.sms.telkom.telkomcrm.repositories;

import com.sms.telkom.telkomcrm.entities.SmsGamasOpen;
import com.sms.telkom.telkomcrm.entities.SmsLabAssign;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SmsGamasOpenRepository extends PagingAndSortingRepository<SmsGamasOpen, Long> {


    Page<SmsGamasOpen> findByMsisdnContaining(String msisdn, Pageable pageable);

    Page<SmsGamasOpen> findByContentToCustomerContaining(String content, Pageable pageable);

}
