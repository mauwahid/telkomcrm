package com.sms.telkom.telkomcrm.repositories;

import com.sms.telkom.telkomcrm.entities.SmsGamasClose;
import com.sms.telkom.telkomcrm.entities.SmsGamasOpen;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SmsGamasCloseRepository extends PagingAndSortingRepository<SmsGamasClose, Long> {

    Page<SmsGamasClose> findByMsisdnContaining(String msisdn, Pageable pageable);

    Page<SmsGamasClose> findByContentToCustomerContaining(String content, Pageable pageable);
}
