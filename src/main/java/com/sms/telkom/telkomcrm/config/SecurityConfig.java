package com.sms.telkom.telkomcrm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;


    private static final String SQL_ROLE
            = "select u.username as username,case when u.role = 1 then 'ROLE_USER' " +
            " when u.role = 2 then 'ROLE_ADMIN' end  as authority "
            + "from user u "
            + "where u.username = ?";

    private static final String SQL_LOGIN
            = "select u.username as username, u.encrypted_password as password, 1 as active "
            + "from user u "
            + "where username = ?";



    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(daoAuthenticationProvider());
    }

    @Bean
    public AuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(passwordEncoder);
        provider.setUserDetailsService(userDetailsService());
        return provider;
    }

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        JdbcDaoImpl userDetails = new JdbcDaoImpl();
        userDetails.setDataSource(dataSource);
        userDetails.setUsersByUsernameQuery(SQL_LOGIN);
        userDetails.setAuthoritiesByUsernameQuery(SQL_ROLE);

        return userDetails;
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

       http.csrf().disable();

        http.headers().frameOptions().sameOrigin();

        http
                .authorizeRequests()
                .antMatchers("/js/**").permitAll()
                .antMatchers("/css/**").permitAll()
                .antMatchers("/fonts/**").permitAll()
                .antMatchers("/img/**").permitAll()
                .antMatchers("/bower_components/**").permitAll()
                .antMatchers("/dist/**").permitAll()
                .antMatchers("/send/**").permitAll()
                .antMatchers("/mo/**").permitAll()


                .antMatchers("/operator/**").hasAuthority("ROLE_ADMIN")
                .antMatchers("/users/**").hasAuthority("ROLE_ADMIN")
                .antMatchers("/dashboard/**").hasAnyAuthority("ROLE_USER", "ROLE_ADMIN")
                .antMatchers("/docapi").hasAnyAuthority("ROLE_USER", "ROLE_ADMIN")
                .antMatchers("/report/**").hasAnyAuthority( "ROLE_USER", "ROLE_ADMIN")
                .antMatchers("/summary/**").hasAnyAuthority( "ROLE_USER", "ROLE_ADMIN")
                .antMatchers("/").authenticated()
                .antMatchers("/send/*").permitAll()
                .antMatchers("/mo/*").permitAll()
                .and()
                .formLogin()
                .loginPage("/login")
                .failureUrl("/login-error")
                .permitAll()
                .and()
                .logout()
                .permitAll();
  //      .anyRequest().permitAll();
    }
}
