package com.sms.telkom.telkomcrm.config;

import com.sms.telkom.telkomcrm.entities.User;
import com.sms.telkom.telkomcrm.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class AppBootstrap implements ApplicationListener<ContextRefreshedEvent> {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;


    private String username = "t3Lkom";
    private String password = "1nfomed1a2018";

    private String usernameAdmin = "adminTelkom";
    private String passAdmin = "Infom3dia@2018";


    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        saveUser();
    }

    private void saveUser(){
        User user = userRepository.findByUsernameAndPassword(username, password);
      //  User user = userRepository.findByUsernameAndEncryptedPassword(username,passwordEncoder.encode(password));

        if(user == null){
            user = new User();
            user.setUsername(username);
            user.setPassword(password);
            user.setEncryptedPassword(passwordEncoder.encode(password));
            user.setRole(1);
            userRepository.save(user);

        }

        User userAdmin = userRepository.findByUsernameAndPassword(usernameAdmin,passAdmin);

        if(userAdmin == null){
            userAdmin = new User();
            userAdmin.setUsername(usernameAdmin);
            userAdmin.setPassword(passAdmin);
            userAdmin.setEncryptedPassword(passwordEncoder.encode(passAdmin));
            userAdmin.setRole(2);
            userRepository.save(userAdmin);

        }


    }
}
